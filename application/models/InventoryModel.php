<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class InventoryModel extends CI_Model {
    
    var $company_id;

    function __construct() {
        parent::__construct();
        $this->company_id=$this->session->user_data->company_id;
    }

    function insert($tableName, $data){
    	$status= $this->db->insert($tableName, $data);
        if($status == true){
            $this->session->set_flashdata('usermsg', "Successfully Inserted !!!");
        }else{
            $this->session->set_flashdata('usermsg', "Failed !!!");
        }
    }

    //get trade list for dropdown box
    function getTrade(){
        $this->db->select('*');
        $this->db->where('company_id', $this->company_id);
        $trade = $this->db->get('inv_trade');
        $data = $trade->result();

        $output = array();
        $output[]='<option value="" disabled selected>Select Trade</option>';
        foreach ($data as $row) {
            $output[]= '<option value="' . $row->trade_id . '">' . $row->trade . '</option>';
        }
        return $output;
    }

    //get trade by param view
    function getAllTradeByParam($jdata){
        $this->db->select('*');
        $this->db->where('company_id', $this->company_id);
            if(!empty($jdata['trade_id'])){
                $this->db->where('trade_id',$jdata['trade_id']);
            }
            if(!empty($jdata['str_src'])){
                $this->db->like('trade',$jdata['str_src']);
            }
        $trade=$this->db->get('inv_trade');
        $data=$trade->result();

        return $data;
    }

    function getBrand(){
        $this->db->select('*');
        $this->db->where('company_id', $this->company_id);
        $brand = $this->db->get('inv_brand');
        $data = $brand->result();

        $output = array();
        $output[]='<option value="" disabled selected>Select Brand</option>';
        foreach ($data as $row) {
            $output[]= '<option value="' . $row->brand_id . '">' . $row->brand . '</option>';
        }
        return $output;
    }
    
    // View total brand page
    function total_brand(){
        $this->db->select('b.brand_id, b.brand, c.category, t.trade');
        $this->db->from('inv_brand as b');
        $this->db->join('inv_category as c', 'b.company_id =c.company_id', 'LEFT');
        $this->db->join('inv_trade as t', 'c.trade_id =t.trade_id', 'LEFT');
        $this->db->where('b.company_id', $this->company_id);
        $brand_view=$this->db->get();
        $data=$brand_view->result();

        return $data;
    }
    //View total brand by parameter
    function getAllBrandByParam($jdata){
        $this->db->select('b.brand_id, b.brand, c.category, t.trade');
        $this->db->from('inv_brand as b');
        $this->db->join('inv_category as c', 'b.company_id =c.company_id', 'LEFT');
        $this->db->join('inv_trade as t', 'b.company_id =t.company_id', 'LEFT');
        $this->db->where('b.company_id', $this->company_id);

            if(!empty($jdata['str_src'])){
                $this->db->like('brand',$jdata['str_src']);
            }   
            if(!empty($jdata['brand_id'])){
                $this->db->where('b.brand_id',$jdata['brand_id']);
            }
            if(!empty($jdata['brand'])){
                $this->db->where('b.brand',$jdata['brand']);
            }
            if(!empty($jdata['category'])){
                $this->db->where('c.category',$jdata['category']);
            }
            if(!empty($jdata['trade'])){
                $this->db->where('c.trade',$jdata['trade']);
            }
        
        $brand_list=$this->db->get();
        $data=$brand_list->result();

        return $data;
    }
    

    function getCategory(){
        $this->db->select('*');
        $this->db->where('company_id', $this->company_id);
        $category = $this->db->get('inv_category');
        $data = $category->result();

        $output = array();
        $output[]='<option value="" disabled selected>Select Category</option>';
        foreach ($data as $row) {
            $output[]= '<option value="' . $row->category_id . '">' . $row->category . '</option>';
        }
        return $output;
    }
    
    // View total category page
    function getAllBrand(){
        $this->db->select('c.category_id, c.category, t.trade');
        $this->db->from('inv_category as c');
        $this->db->join('inv_trade as t', 'c.company_id =t.company_id', 'INNER');
        $this->db->where('c.company_id', $this->company_id);
        $category_view=$this->db->get();
        $data=$category_view->result();

        return $data;
    }

    // View total brand page
    function total_category(){
        $this->db->select('c.category_id, c.category, t.trade');
        $this->db->from('inv_category as c');
        $this->db->join('inv_trade as t', 'c.trade_id =t.trade_id', 'LEFT');
        $this->db->where('c.company_id', $this->company_id);
        $category_view=$this->db->get();
        $data=$category_view->result();

        return $data;
    }

    //View total category by parameter
    function getAllCategoryByParam($jdata){
        $this->db->select('c.category_id, c.category, t.trade');
        $this->db->from('inv_category as c');
        $this->db->join('inv_trade as t', 'c.trade_id= t.trade_id', 'INNER');
        $this->db->where('c.company_id', $this->company_id);

            if(!empty($jdata['str_src'])){
                $this->db->like('trade',$jdata['str_src']);
            }   
            if(!empty($jdata['category_id'])){
                $this->db->where('c.category_id',$jdata['category_id']);
            }
            if(!empty($jdata['category'])){
                $this->db->where('c.category',$jdata['category']);
            }
            if(!empty($jdata['trade'])){
                $this->db->where('c.trade',$jdata['trade']);
            }
        
        $category_list=$this->db->get();
        $data=$category_list->result();

        return $data;
       }

    function getUnit(){
        $this->db->select('*');
        $this->db->where('company_id', $this->company_id);
        $unit = $this->db->get('inv_unit');
        $data = $unit->result();

        $output = array();
        $output[]='<option value="" disabled selected>Select Unit</option>';
        foreach ($data as $row) {
            $output[]= '<option value="' . $row->unit_id . '">' . $row->unit . '</option>';
        }
        return $output;
    }
    
    //get product list for dropdown, view product page
    function getProductList($category_id){
        $this->db->select('*');
        $this->db->where('company_id', $this->company_id);
        if($category_id>0){
            $this->db->where('category_id',$category_id);
        }
        $products = $this->db->get('inv_products');
        $data = $products->result();

        $output = array();
        $output[]='<option value="" disabled selected>Select Category</option>';
        foreach ($data as $row) {
            $output[]= '<option value="' . $row->product_id . '">' . $row->product . '</option>';
        }
        return $output;
    }

    //get product details from product table for view product page
    function getProducts($category_id, $product_id){
        $this->db->select('*');
        $this->db->where('company_id', $this->company_id);
        if($category_id>0){
            $this->db->where('category_id',$category_id);
        }
        if($product_id>0){
            $this->db->where('product_id',$product_id);
        }
        $products=$this->db->get('inv_products');
        $data=$products->result();

        return $data;
    }
}