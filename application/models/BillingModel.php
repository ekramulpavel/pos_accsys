<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class BillingModel extends CI_Model {
    
    var $company_id;

    function __construct() {
        parent::__construct();
        $this->company_id=$this->session->user_data->company_id;
    }

    function insert($tableName, $data){
    	$status= $this->db->insert($tableName, $data);
        if($status == true){
            $this->session->set_flashdata('usermsg', "Successfully Inserted !!!");
        }else{
            $this->session->set_flashdata('usermsg', "Failed !!!");
        }
    }

    function update($table, $inputall){
        $this->db->where('company_id', $this->company_id);
        $this->db->where('client_id', $inputall['client_id']);
        $status = $this->db->update($table, $inputall);
        if($status == true){
            $this->session->set_flashdata('usermsg', "Successfully Update !!!");
        }else{
            $this->session->set_flashdata('usermsg', "Failed !!!");
        }
    }

    function total_client_by_package(){
        $this->db->select('p.pkg_name, p.pkg_rate, count(c.client_id)as Total_Client');
        $this->db->from('bill_pkg as p');
        $this->db->join('bill_client as c', 'p.pkg_id=c.pkg_id', 'INNER');
        $this->db->where('p.company_id', $this->company_id);
        $this->db->where('c.status', 1);
        $this->db->group_by('p.pkg_id');
        $this->db->order_by('p.pkg_rate','desc');
        $pkg_list=$this->db->get();
        $data=$pkg_list->result();

        return $data;
    }

    function total_client_by_area(){
        $this->db->select('a.area_id, a.area, count(c.client_id)as Total_Client');
        $this->db->from('bill_area as a');
        $this->db->join('bill_client as c', 'a.area_id=c.area_id', 'INNER');
        $this->db->where('a.company_id', $this->company_id);
        $this->db->where('c.status', 1);
        $this->db->group_by('a.area_id');
        $pkg_list=$this->db->get();
        $data=$pkg_list->result();

        return $data;
    }

    
    function getAllClientByParam($jdata){
        if (isset($jdata['str_src'])){
            $str=$jdata['str_src'];
        }
        $this->db->select('c.client_id, c.client_name, c.nid, c.ip_card, c.address, c.opening_balance, c.conn_charge, c.con_date, c.mobile, p.pkg_id, p.pkg_name, ct.conn_id, ct.conn_type, s.id, s.status, a.area_id, a.area, m.managed_id, m.managed_by, c.remarks');
        $this->db->from('bill_client AS c');
        $this->db->join('bill_pkg AS p','c.pkg_id=p.pkg_id','LEFT');
        $this->db->join('bill_connection_type AS ct','c.conn_type=ct.conn_id','LEFT');
        $this->db->join('bill_conn_status AS s','c.status=s.id','LEFT');
        $this->db->join('bill_area AS a','c.area_id=a.area_id','LEFT');
        $this->db->join('bill_managed_by AS m','c.managed_by=m.managed_id','LEFT');
        $this->db->where('c.company_id', $this->company_id);

            if(!empty($jdata['client_id'])){
                $this->db->where('c.client_id',$jdata['client_id']);
            }
            if(!empty($jdata['conn_type'])){
                $this->db->where('c.conn_type',$jdata['conn_type']);
            }
            if(!empty($jdata['pkg_id'])){
                $this->db->where('c.pkg_id',$jdata['pkg_id']);
            }
            if(!empty($jdata['area_id'])){
                $this->db->where('c.area_id',$jdata['area_id']);
            }
            if(!empty($jdata['managed_by'])){
                $this->db->where('c.managed_by',$jdata['managed_by']);
            }
            if(!empty($jdata['status'])){
                $this->db->where('c.status',$jdata['status']);
            }
            if(!empty($str)){
                $where="(c.client_id LIKE '%$str%' OR c.client_name LIKE '%$str%' OR c.ip_card LIKE '%$str%' OR c.mobile LIKE '%$str%' OR p.pkg_name LIKE '%$str%' OR ct.conn_type LIKE '%$str%' OR s.status LIKE '%$str%' OR m.managed_by LIKE '%$str%')";
                $this->db->where($where);
            }
        $this->db->order_by('c.con_date');

        $client_list = $this->db->get();
        $data = $client_list->result();
        return $data;

    }

    function getClientID(){
        $this->db->select('*');
        $this->db->where('company_id', $this->company_id);
        $trade = $this->db->get('bill_client');
        $data = $trade->result();

        $output = array();
        $output[]='<option value="" disabled selected>Select Client</option>';
        foreach ($data as $row) {
            $output[]= '<option value="' . $row->client_id . '">' . $row->client_id . '</option>';
        }
        return $output;
    }

    function getPackage(){
        $this->db->select('*');
        $this->db->where('company_id', $this->company_id);
        $trade = $this->db->get('bill_pkg');
        $data = $trade->result();

        $output = array();
        $output[]='<option value="" disabled selected>Select Package</option>';
        foreach ($data as $row) {
            $output[]= '<option value="' . $row->pkg_id . '">' . $row->pkg_name . '</option>';
        }
        return $output;
    }

    function getConnectionType(){
        $this->db->select('*');
        $this->db->where('company_id', $this->company_id);
        $brand = $this->db->get('bill_connection_type');
        $data = $brand->result();

        $output = array();
        $output[]='<option value="" disabled selected>Select Connection Type</option>';
        foreach ($data as $row) {
            $output[]= '<option value="' . $row->conn_id . '">' . $row->conn_type . '</option>';
        }
        return $output;
    }

    function getStatus(){
        $this->db->select('*');
        $this->db->where('company_id', $this->company_id);
        $category = $this->db->get('bill_conn_status');
        $data = $category->result();

        $output = array();
        $output[]='<option value="" disabled selected>Select Connection Status</option>';
        foreach ($data as $row) {
            $output[]= '<option value="' . $row->id . '">' . $row->status . '</option>';
        }
        return $output;
    }

    function getArea(){
        $this->db->select('*');
        $unit = $this->db->get('bill_area');
        $data = $unit->result();

        $output = array();
        $output[]='<option value="" disabled selected>Select Area</option>';
        foreach ($data as $row) {
            $output[]= '<option value="' . $row->area_id . '">' . $row->area . '</option>';
        }
        return $output;
    }

    function getManaged_by(){
        $this->db->select('*');
        $unit = $this->db->get('bill_managed_by');
        $data = $unit->result();

        $output = array();
        $output[]='<option value="" disabled selected>Select Managed By</option>';
        foreach ($data as $row) {
            $output[]= '<option value="' . $row->managed_id . '">' . $row->managed_by . '</option>';
        }
        return $output;
    }

    function check_client_validity_for_payment($company_id, $client_id){
        $this->db->select('due_month, amount_due');
        $this->db->from('bill_rent');
        $this->db->where('client_id',$client_id);
        $this->db->where('company_id',$company_id);
        $this->db->order_by('due_month','desc');
        $this->db->limit(1);
        $due = $this->db->get()->result_array();
        
        $this->db->select('c.client_id, c.client_name, p.pkg_name');
        $this->db->from('bill_client as c');
        $this->db->join('bill_pkg as p', 'c.pkg_id=p.pkg_id','INNER');
        $this->db->where('c.client_id',$client_id);
        $this->db->where('c.company_id',$company_id);
        $client_info = $this->db->get()->result_array();
        
        $data=array_merge($client_info,$due);
        return $data;
    }

    //============================Client Report Model=============================//
    function client_details_param($client_id, $company_id, $from, $to){
        $status= $this->db->query("CALL bill_client_report('{$client_id}', '{$company_id}', '{$from}', '{$to}')"); 
        $data=$status->result();

        return $data;
    }
}