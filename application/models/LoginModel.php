<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class LoginModel extends CI_Model {
    
    function __construct() {
        parent::__construct();
    }

    function auth_user($username, $password) {
        $this->db->where('email', $username);
        $this->db->where('password', md5($password));
        $query = $this->db->get('sys_user');
        $valid = $query->result();

        $this->db->select('c.company_id, c.company, u.user_name, u.email, u.user_role, ur.user_role as designation');
        $this->db->from('sys_user AS u');
        $this->db->join('sys_company_info AS c','c.company_id=u.company_id','LEFT');
        $this->db->join('sys_employee_info AS e','e.company_id=u.company_id','LEFT');
        $this->db->join('sys_user_role AS ur','ur.id=u.user_role','INNER');
        $this->db->where('u.email', $username);
        $query = $this->db->get();

        $data=$query->result();                                                                                        

        if (empty($valid)) {
            return FALSE;
        } else {
            $this->session->set_userdata('user_data', $data[0]);
            return TRUE;
        }
    }

    function insert_log(){
        $date=date("Y-m-d H:i:s", time());
        $data = array(
            'user' => $this->session->user_data->email,
            'in_time'=> $date,
            'ip_address'=> $this->input->ip_address()
        );
        $this->db->insert('sys_log_info', $data);
    }

    function update_log(){
        $date=date("Y-m-d H:i:s", time());
        $user = $this->session->user_data->email;
                
        $this->db->set('out_time', $date);
        $this->db->where('user', $user);
        $this->db->order_by('in_time','DESC');
        $this->db->limit(1); 
        $this->db->update('sys_log_info');
        
    }

    function change_password($password){
        $msg=null; 
        $email=$this->session->user_data->email;
        $this->db->where('Email', $email);
        $this->db->update('sys_user', $password);    
    }

    function getProjects(){
        $company_id=$this->session->user_data->company_id;
        $this->db->where('company_id',$company_id);
        $store = $this->db->get('sys_company_lic');
        $data = $store->result();

        $projects = array();
        foreach ($data as $row) {
            $projects[]='<a href="'.base_url().$row->application.'" class="w3-padding"><i class="fa fa-bullseye fa-fw"></i>  '.$row->application.'</a>';
        }
        return $projects;
    }

}