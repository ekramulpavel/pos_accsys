<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class getMaxId extends CI_Model {
    
    function __construct() {
        parent::__construct();
    }

    function getMaxId_ByCompany($fieldName, $tableName){
    	$company_id=$this->session->user_data->company_id;
    	$this->db->select_max($fieldName,'id');
    	$this->db->where('company_id', $company_id);
    	$result=$this->db->get($tableName)->row();

    	return $result;
    }

    function getMaxProductId_ByCompany($category){
        $company_id=$this->session->user_data->company_id;
        $this->db->select('product_id as id');
        $this->db->where('company_id', $company_id);
        $this->db->where('category_id', $category);
        $this->db->order_by('product_id', 'desc');
        $this->db->limit(1);
        $result=$this->db->get('inv_products')->row();

        return $result;
    }
}