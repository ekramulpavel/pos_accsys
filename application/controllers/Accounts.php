<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Accounts extends MY_Controller {

    function __construct() {
        parent::__construct();
        if(!is_logged_in()){
            redirect('Login');
        }
    }
    
	public function index()	{
		$data['title']='Acounting System';
		$data['sidenav']='accounts/navbar';
		$data['content']='accounts/dashboard';

		$this->load->view($this->layout,$data);
	}
}