<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

    function __construct() {
        parent::__construct();
        if(!is_logged_in()){
            redirect('Login');
        }
        $this->load->model('LoginModel');
    }
    
	public function index()	{
		$data['title']='Dashboard';
		$data['sidenav']='layout/navbar';
		$data['projects']=$this->LoginModel->getProjects();
		$data['content']='layout/dashboard';

		$this->load->view($this->layout,$data);
	}
}