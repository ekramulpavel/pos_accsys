<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Billing extends MY_Controller {

    function __construct() {
        parent::__construct();
        if(!is_logged_in()){
            redirect('Login');
        }
        $this->load->model('BillingModel');
    }
    
	function index()	{
		$data['title']='Billing System';
		$data['pkg_list']= $this->BillingModel->total_client_by_package();
		$data['area_list']= $this->BillingModel->total_client_by_area();
		$data['sidenav']='billing/navbar';
		$data['content']='billing/dashboard';

		$this->load->view($this->layout,$data);
	}

	//====================================view page for insert  new client===========================================//
	function insert_client(){
		$table='bill_client';
		$data['title']='Billing';
		$data['form_title']='Insert New Client';
		$data['msg'] = $this->session->flashdata('usermsg');
		$data['table']=$table;
		$data['pkg']= $this->BillingModel->getPackage();
		$data['conn_type']= $this->BillingModel->getConnectionType();
		$data['status']= $this->BillingModel->getStatus();
		$data['area']= $this->BillingModel->getArea();
		$data['managed_by']= $this->BillingModel->getManaged_by();

		$data['sidenav']='billing/navbar';
		$data['content']='billing/insert_client';

		$this->load->view($this->layout,$data);
	}

	//check duplicate client id before insert new client
	function chk_duplicate_client_id(){
		$client_id= $this->input->post('client_id');
		$status=$this->db->where('client_id', $client_id)->get('bill_client')->result();
		if ($status==true){
			echo "Duplicate Client ID";
		}
	}

	//insert  new client
	function insert(){
		$company_id=$this->session->user_data->company_id;
		$path="uploads/billing-client/";

		$input=$this->input->post();
		$table=$input['table'];
		$id=$input['client_id'];
		$photo=$path.$company_id.'-'.$id.'.jpg';
		$photo_name=$company_id.'-'.$id.'.jpg';

			isset($input['table']);
			unset($input['table']);
			unset($input['userfile']);
		
		$defaul=array(
			'company_id' =>$company_id,
			'photo'		=>$photo
			);

		$inputall=array_merge($input,$defaul);
		
		$status=$this->db->where('client_id', $id)->get('bill_client')->result();
		if($status==false){
			$this->BillingModel->insert($table, $inputall);
			move_uploaded_file($_FILES['userfile']['tmp_name'],  $path. $photo_name);
		}else{
			$this->session->set_flashdata('usermsg', "Faile to insert: Duplicate Client ID !!!");
		}
		header('Location: ' . $_SERVER['HTTP_REFERER']);
		
	}

	//====================================view page for all client===========================================//
	function view_client()	{
		$jdata=null;

		$table='bill_client';
		$data['title']='Billing';
		$data['form_title']='Client List By Parameter';
		$data['msg'] = $this->session->flashdata('usermsg');
		$data['table']=$table;
		$data['client']= $this->BillingModel->getClientID();
		$data['pkg']= $this->BillingModel->getPackage();
		$data['conn_type']= $this->BillingModel->getConnectionType();
		$data['status']= $this->BillingModel->getStatus();
		$data['area']= $this->BillingModel->getArea();
		$data['managed_by']= $this->BillingModel->getManaged_by();
		$data['client_list']=$this->BillingModel->getAllClientByParam($jdata);

		$data['sidenav']='billing/navbar';
		$data['content']='billing/view_client';

		$this->load->view($this->layout,$data);
	}

	//view client by parameter
	function viewClientByParam(){
		$jdata= json_decode($_POST['data'], true);
		$data = $this->BillingModel->getAllClientByParam($jdata);
		echo json_encode($data);		
	}


	//====================================edit view for existing client====================================//
	function edit_client()	{
		$jdata=array(
			'client_id'=>$this->uri->segment(4)
			); 
		$table='bill_client';
		$data['title']='Billing';
		$data['form_title']='Edit individual client info';
		$data['msg'] = $this->session->flashdata('usermsg');
		$data['table']=$table;
		$data['client']= $this->BillingModel->getClientID();
		$data['pkg']= $this->BillingModel->getPackage();
		$data['conn_type']= $this->BillingModel->getConnectionType();
		$data['status']= $this->BillingModel->getStatus();
		$data['area']= $this->BillingModel->getArea();
		$data['managed_by']= $this->BillingModel->getManaged_by();
		$data['client_list']=$this->BillingModel->getAllClientByParam($jdata);

		$data['sidenav']='billing/navbar';
		$data['content']='billing/edit_client';

		$this->load->view($this->layout,$data);
	}

	//update client info
	function update_client(){
		$company_id=$this->session->user_data->company_id;
		$path="uploads/billing-client/";

		$input=$this->input->post();
		$table=$input['table'];
		$id=$input['client_id'];
		$photo=$path.$company_id.'-'.$id.'.jpg';
		$photo_name=$company_id.'-'.$id.'.jpg';

			isset($input['table']);
			unset($input['table']);
			unset($input['userfile']);
		
		$defaul=array(
			'company_id' =>$company_id,
			'photo'		=>$photo
			);

		$inputall=array_merge($input,$defaul);

		$status = $this->BillingModel->update($table, $inputall);
		if($status==true){
			move_uploaded_file($_FILES['userfile']['tmp_name'],  $path. $photo_name);
		}
		header('Location: ' . $this->view_client());
	}

	//====================================delete client===========================================//
	function del(){
		$table=$this->uri->segment(3);
		$id= $this->uri->segment(4);
		$this->db->where('client_id', $id)->delete($table);
		$this->session->set_flashdata('usermsg', "Successfully Deleted !!!");
		header('Location: ' . $_SERVER['HTTP_REFERER']);
	}

	
	//====================================view page for client payment entry====================================//
	function view_client_payment()	{
		$table='bill_rent';
		$data['title']='Billing';
		$data['form_title']='Insert Client Payment';
		$data['msg'] = $this->session->flashdata('usermsg');
		$data['table']=$table;
		
		$data['sidenav']='billing/navbar';
		$data['content']='billing/insert_payment';

		$this->load->view($this->layout,$data);
	}

	//check if client id is valid yet or not
	function check_client_validity(){
		$company_id = $this->session->user_data->company_id;
		$client_id  = $this->input->post('client_id', TRUE);
		
		$data=$this->BillingModel->check_client_validity_for_payment($company_id, $client_id);
		if($data==true){
			echo json_encode($data);
		}else{
			$this->session->set_flashdata('usermsg', "You entered an invalid Client ID  !!!");
			//header('Location: ' . $_SERVER['HTTP_REFERER']);
		}
	}


	//insert or update bill_rent table
	function insert_client_payment(){
		$inputall=$this->input->post();
		$table=$inputall['table'];
		$data=array(
			'pay_date'		=>$inputall['date'],
			'amount_paid'	=>$inputall['amount_paid']
			);

		//check last month paid amount 0
		$chk_month=$this->db->select('due_month')->where('company_id',$inputall['company_id'])->where('client_id',$inputall['client_id'])->where('amount_paid<',1)->order_by('due_month','desc')->limit(1)->get('bill_rent')->row();
		
		$ym=date('F Y', strtotime($inputall['due_month']));
		$chk=date('F Y', strtotime($chk_month->due_month));
		
		if($ym==$chk){
			$this->db->where('company_id',$inputall['company_id']);
			$this->db->where('client_id',$inputall['client_id']);
			$this->db->where('due_month',$inputall['due_month']);
			$update=$this->db->update($table, $data);
			if($update==true){
				$this->session->set_flashdata('usermsg',"Payment Update Successfully !!!");
				header('Location: ' . $_SERVER['HTTP_REFERER']);
			}
		}else{
			$data=array(
				'company_id'=> $inputall['company_id'],
				'client_id'	=> $inputall['client_id'],
				'due_month'	=> '',
				'pay_date'	=> $inputall['date'],
				'amount_due'=> '0',
				'amount_paid'=>$inputall['amount_paid']
				);
			
			$insert=$this->db->insert($table, $data);
			
			if($insert==true){
				$this->session->set_flashdata('usermsg',"Payment Update Successfully !!!");
				header('Location: ' . $_SERVER['HTTP_REFERER']);
			}
		}
	}

	//====================================view page for insert  new package===========================================//
	function insert_view_pkg(){
		$table='bill_pkg';
		$data['title']='Billing';
		$data['form_title']='Insert New Package';
		$data['msg'] = $this->session->flashdata('usermsg');
		$data['table']=$table;
		$data['max']= $this->getMaxId->getMaxId_ByCompany('pkg_id', $table);
		$data['data']=$this->db->get($table)->result();

		$data['sidenav']='billing/navbar';
		$data['content']='billing/insert_view_pkg';

		$this->load->view($this->layout,$data);
	}	

	//====================================view page for insert  new area===========================================//
	function insert_view_area(){
		$table='bill_area';
		$data['title']='Billing';
		$data['form_title']='Insert New Area';
		$data['msg'] = $this->session->flashdata('usermsg');
		$data['table']=$table;
		$data['max']= $this->getMaxId->getMaxId_ByCompany('area_id', $table);
		$data['data']=$this->db->get($table)->result();

		$data['sidenav']='billing/navbar';
		$data['content']='billing/insert_view_area';

		$this->load->view($this->layout,$data);
	}

	//====================================view page for insert  new manager===========================================//
	function insert_view_manager(){
		$table='bill_managed_by';
		$data['title']='Billing';
		$data['form_title']='Insert New Manager';
		$data['msg'] = $this->session->flashdata('usermsg');
		$data['table']=$table;
		$data['max']= $this->getMaxId->getMaxId_ByCompany('managed_id', $table);
		$data['data']=$this->db->get($table)->result();

		$data['sidenav']='billing/navbar';
		$data['content']='billing/insert_view_manager';

		$this->load->view($this->layout,$data);
	}

	//====================================view page for insert  new connection type===========================================//
	function insert_view_conn_type(){
		$table='bill_connection_type';
		$data['title']='Billing';
		$data['form_title']='Insert New Connection Type';
		$data['msg'] = $this->session->flashdata('usermsg');
		$data['table']=$table;
		$data['max']= $this->getMaxId->getMaxId_ByCompany('conn_id', $table);
		$data['data']=$this->db->get($table)->result();

		$data['sidenav']='billing/navbar';
		$data['content']='billing/insert_view_conn_type';

		$this->load->view($this->layout,$data);
	}

	//insert  all others form (common function)
	function insert_all(){
		$input=$this->input->post();
		$table=$input['table'];
			isset($input['table']);
			unset($input['table']);

		$this->BillingModel->insert($table, $input);
		header('Location: ' . $_SERVER['HTTP_REFERER']);
	}

	//====================================Report Section - Client Details===========================================//
	function client_deatails_param(){
		$data['title']='Billing';
		$data['form_title']='Client Details Report';
		$data['msg'] = $this->session->flashdata('usermsg');

		$data['sidenav']='billing/navbar';
		$data['content']='billing/client_deatails';

		$this->load->view($this->layout,$data);
	}

	function client_deatails(){
		$jdata= json_decode($_POST['data'], true);

		$company_id= $this->session->user_data->company_id;
		$client_id= $jdata['client_id'];
		$from= $jdata['from'];
		$to= $jdata['to'];
		
		if(empty($from)){
			$from='2000-01-01';
		}

		if(empty($to)){
			$to=date('Y-m-d');
		}
		
		$data=$this->BillingModel->client_details_param($client_id, $company_id, $from, $to);
		//$this->output->set_header('Content-Type: application/json; charset=utf-8');
		return json_encode($data);
		//print_r($data);
		//return $data;
	}
}