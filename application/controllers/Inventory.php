<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inventory extends MY_Controller {

    function __construct() {
        parent::__construct();
        if(!is_logged_in()){
            redirect('Login');
        }
        $this->load->model('InventoryModel');
    }
    
	public function index()	{
		$data['title']='Inventory';
		$data['sidenav']='inventory/navbar';
		$data['content']='inventory/dashboard';

		$this->load->view($this->layout,$data);
	}

	//common function for all insertion
	function insert(){
		$input=$this->input->post();
		$table=$input['table'];

			isset($input['table']);
			unset($input['table']);

		$this->InventoryModel->insert($table, $input);
		header('Location: ' . $_SERVER['HTTP_REFERER']);
	}

	//======================================================TRADE======================================================//
	//new trade insert page view-page
	function insert_trade(){
		$table='inv_trade';
		$data['title']='Inventory';
		$data['form_title']='Insert New Trade';
		$data['msg'] = $this->session->flashdata('usermsg');
		$data['table']=$table;
		$data['max']= $this->getMaxId->getMaxId_ByCompany('trade_id', $table);
		$data['sidenav']='inventory/navbar';
		$data['content']='inventory/insert_trade';

		$this->load->view($this->layout,$data);
	}

	//view all trade list on load
	function view_trade(){
		$table='inv_trade';
		$data['title']='Inventory';
		$data['form_title']='View Trade';
		$data['trade']= $this->InventoryModel->getTrade();
		$data['trade_view']= $this->db->where('company_id',$this->session->user_data->company_id)->get('inv_trade')->result();
		$data['table']=$table;
		$data['sidenav']='inventory/navbar';
		$data['content']='inventory/view_trade';

		$this->load->view($this->layout,$data);
	}

	//view trade list by param
	function viewTradeByParam(){
		$jdata= json_decode($_POST['data'], true);
		$data = $this->InventoryModel->getAllTradeByParam($jdata);
		echo json_encode($data);		
	}


	//======================================================CATEGORY======================================================//
	//new catergory insert view-page
	function insert_category()	{
		$table='inv_category';
		$data['title']='Inventory';
		$data['form_title']='Insert New Category';
		$data['msg'] = $this->session->flashdata('usermsg');
		$data['table']=$table;
		$data['max']= $this->getMaxId->getMaxId_ByCompany('category_id', $table);
		$data['trade']= $this->InventoryModel->getTrade();
		$data['sidenav']='inventory/navbar';
		$data['content']='inventory/insert_category';

		$this->load->view($this->layout,$data);
	}

	//view all category list on load
	function view_category(){
		$table='inv_category';
		$data['title']='Inventory';
		$data['form_title']='View Category';
		$data['category']= $this->InventoryModel->getCategory();
		$data['category_view']= $this->InventoryModel->total_category();
		$data['table']=$table;
		$data['sidenav']='inventory/navbar';
		$data['content']='inventory/view_category';

		$this->load->view($this->layout,$data);
	}

	//view category list by param
	function viewCategoryByParam(){
		$jdata= json_decode($_POST['data'], true);
		$data = $this->InventoryModel->getAllCategoryByParam($jdata);
		echo json_encode($data);		
	}

	//======================================================PRODUCTS======================================================//
	//new products insert view-page
	function insert_products()	{
		$table='inv_products';
		$data['title']='Inventory';
		$data['form_title']='Insert New Products';
		$data['msg'] = $this->session->flashdata('usermsg');
		$data['table']=$table;
		$data['brand']= $this->InventoryModel->getBrand();
		$data['category']= $this->InventoryModel->getCategory();
		$data['unit']= $this->InventoryModel->getUnit();
		$data['sidenav']='inventory/navbar';
		$data['content']='inventory/insert_products';

		$this->load->view($this->layout,$data);
	}

	//generate product_id by category
	function getProduct_id(){
		$category_id = $this->input->post('category_id', TRUE);
		$product_id= $this->getMaxId->getMaxProductId_ByCompany($category_id);
		$id='a';
			if (isset($product_id)) {
				$id=substr($product_id->id,-1);		
				$id++;
			}
		echo $category_id.$id;
	}

	//view all products list on load
	function view_products(){
		$category_id=null;
		$product_id=null;
		$data['title']='Inventory';
		$data['form_title']='View Products';
		$data['category']= $this->InventoryModel->getCategory();
		$data['product_list']= $this->InventoryModel->getProductList($category_id);
		$data['products']= $this->InventoryModel->getProducts($category_id, $product_id);
		$data['sidenav']='inventory/navbar';
		$data['content']='inventory/view_products';

		$this->load->view($this->layout,$data);
	}

	//product drop down list for products view page
	function getProductListByCategory(){
		$category_id = $this->input->post('category_id');
		$product_id=null;
		$data['list']= $this->InventoryModel->getProductList($category_id);
		$data['products']= $this->InventoryModel->getProducts($category_id, $product_id);
		//header('Content-type: application/json');
		echo json_encode($data);
	}

	function product_details(){
		$data['title']='Inventory';
		$data['form_title']='Product Details';
		$data['sidenav']='inventory/navbar';
		$data['content']='inventory/product_details';

		$this->load->view($this->layout,$data);
	}

	//======================================================ORDER======================================================//
	function insert_order()	{
		$data['title']='Inventory';
		$data['sidenav']='inventory/navbar';
		$data['content']='inventory/insert_order';

		$this->load->view($this->layout,$data);
	}


	//======================================================BRAND======================================================//
	//new brand insert view-page
	function insert_brand()	{
		$table='inv_brand';
		$data['title']='Inventory';
		$data['form_title']='Insert New Brand';
		$data['msg'] = $this->session->flashdata('usermsg');
		$data['table']=$table;
		$data['max']= $this->getMaxId->getMaxId_ByCompany('brand_id', $table);
		$data['sidenav']='inventory/navbar';
		$data['content']='inventory/insert_brand';

		$this->load->view($this->layout,$data);
	}

	//view all brand list on load
	function view_brand(){
		$table='inv_brand';
		$data['title']='Inventory';
		$data['form_title']='View Brand';
		$data['category']= $this->InventoryModel->getCategory();
		$data['brand_view']= $this->InventoryModel->getAllBrandByParam();
		$data['table']=$table;
		$data['sidenav']='inventory/navbar';
		$data['content']='inventory/view_brand';

		$this->load->view($this->layout,$data);
	}

	//view brand list by param
	function viewBrandByParam(){
		$jdata= json_decode($_POST['data'], true);
		$data = $this->InventoryModel->getAllBrandByParam($jdata);
		echo json_encode($data);		
	}
	
}