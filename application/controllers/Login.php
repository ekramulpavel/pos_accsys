<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('LoginModel');
    }
    
	public function index()	{
		$data['msg'] = $this->session->flashdata('usermsg');
		$this->load->view('login/login', $data);
	}

	function check(){
        $username=  $this->input->post('email');
        $password=  $this->input->post('password');
        
        $status=  $this->LoginModel->auth_user ($username,$password);
        
        if($status == true){
			$this->LoginModel->insert_log();
			redirect('home');
        }else{
			$this->session->set_flashdata('usermsg', "Failed to Authenticate !!!");
			redirect('login');
        }
    }

    function logout(){
		// Logout Code  Here
		$userdata = $this->session->userdata('user_data');	
		
		if(!empty($userdata)){
			$this->LoginModel->update_log();
			$this->session->unset_userdata('user_data');
		}
		//Redirect to Default Page
		redirect('Login');
	}
}