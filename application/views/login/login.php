<!DOCTYPE html>
<html>
  <title>AccSys - Login</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">

  <style type="text/css">
    html, body {
      height: 100%;
    }

    html {
      display: table;
      margin: auto;
    }

    body {
      display: table-cell;
      vertical-align: middle;
    }
  </style>

<body class="w3-container">
  <div id="id01">
    <div class="w3-modal-content w3-card-8 w3-animate-zoom" style="max-width:500px">
    
      <div class="w3-center">
        <div class="w3-panel w3-pale-teal w3-bottombar w3-border-teal w3-border">
          <h4 class="w3-text-teal">Login to your Account</h4>
        </div>
        <h6 class="w3-text-red"><?php echo isset($msg)? "$msg": NULL;?>
        </h6>
      </div>
      <form class="w3-container" action="<?php echo base_url(); ?>login/check/" method="post">
        <div class="w3-section">
          <label><b>Email Address</b></label>
          <input class="w3-input w3-border w3-margin-bottom" type="email" placeholder="Enter your email address to login" name="email" required>

          <label><b>Password</b></label>
          <input class="w3-input w3-border" type="password" placeholder="Enter Password" name="password" required>

          <button class="w3-btn-block w3-teal w3-section w3-padding" type="submit">Login</button>
          <input class="w3-check w3-margin-top" type="checkbox" checked="checked"> Remember me
        </div>
      </form>

      <div class="w3-container w3-border-top w3-padding-16 w3-light-grey">
        <button onclick="document.getElementById('id01').style.display='none'" type="button" class="w3-left w3-btn w3-dark-grey">Cancel</button>
        <span class="w3-right w3-padding w3-hide-small">New <a href="<?php echo base_url(); ?>registration">register</a></span>
        <span class="w3-right w3-padding">Forgot <a href="#">password?</a></span>
      </div>
    </div>
  </div>
            
</body>
</html>

