<!DOCTYPE html>
<html>
    <title>AccSys</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
    
    <style>
        html,body,h1,h2,h3,h4,h5 {font-family: "Raleway", sans-serif}
    </style>

<body class="w3-light-grey">

    <!-- Top container -->
    <div class="w3-container w3-black w3-large w3-padding-16 w3-text-blue" style="z-index:4;">
        <span class="w3-left w3-hide-small w3-hide-medium">VIVIDWORK Ltd. - AccSys</span>
        <span class="w3-right">Logo</span>
    </div>

    <div class="w3-container">
        <div class="w3-panel w3-pale-teal w3-bottombar w3-border-teal w3-border">
            <form class="w3-container">
            <br>
                <div class="w3-container w3-teal">
                    <h4>Registration Form</h4>
                </div>
                <br>
                <div class="w3-row-padding">
                    <div class="w3-col s12 m12 l3">
                        <label>Company Name (In short form)</label>
                        <input class="w3-input w3-border w3-round w3-hover-sand" type="text">
                    </div>
                    <div class="w3-col s12 m12 l9">
                        <label>Company Name</label>
                        <input class="w3-input w3-border w3-round w3-hover-sand" type="text">
                    </div>
                </div>
                <br>
                <div class="w3-row-padding">
                    <div class="w3-col s12 m12 l6">
                        <label>Owner Name</label>
                        <input class="w3-input w3-border w3-round w3-hover-sand" type="text">
                    </div>
                    <div class="w3-col s12 m12 l6">
                        <label>Registered Address</label>
                        <input class="w3-input w3-border w3-round w3-hover-sand" type="text">
                    </div>
                </div>
                <br>
                <div class="w3-row-padding">
                    <div class="w3-col s12 m12 l6">
                        <label>Mobile Number</label>
                        <input class="w3-input w3-border w3-round w3-hover-sand" type="number">
                    </div>
                    <div class="w3-col s12 m12 l6">
                        <label>Phone Number</label>
                        <input class="w3-input w3-border w3-round w3-hover-sand" type="number">
                    </div>
                </div>
                
                <br>
                <div class="w3-container w3-teal">
                    <h4>Credential</h4>
                </div>
                <div class="w3-panel w3-pale-yellow w3-leftbar w3-border-yellow">
                    <p>Note: Please enter a valid email address because an email notification will be sent to you to activate this account. Moreover, if you forgot your password then it can also be retrieved through this email address.</p>
                </div>
                <div class="w3-row-padding">
                    <div class="w3-col s12 m12 l6">
                        <label>Email</label>
                        <input class="w3-input w3-border w3-round w3-hover-sand" type="email">
                    </div>
                    <div class="w3-col s12 m12 l6">
                        <label>Confirm Email</label>
                        <input class="w3-input w3-border w3-round w3-hover-sand" type="email">
                    </div>
                </div>
                <div class="w3-row-padding">
                    <div class="w3-col s12 m12 l6">
                        <label>Password</label>
                        <input class="w3-input w3-border w3-round w3-hover-sand" type="password">
                    </div>
                    <div class="w3-col s12 m12 l6">
                        <label>Confirm Password</label>
                        <input class="w3-input w3-border w3-round w3-hover-sand" type="password">
                    </div>
                </div>
                <br>
                <div class="w3-row-padding">
                    <div class="w3-col s12 m12 l12">
                        <button class="w3-btn w3-teal w3-right" type="submit">Submit</button>
                    </div>
                </div>
                
            <br>    
            </form>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            $(".match").blur(function () {
                if($("#email").val() != $("#confirm_email").val()){
                    alert("Emails not matched");
                    $("#confirm_email").focus();
                }
                if($("#pwd").val() != $("#confirm_pwd").val()){
                    alert("Password not matched");
                    $("#confirm_pwd").focus();
                }
            });
        });
    </script>

</body>
</html>