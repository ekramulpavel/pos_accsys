<!DOCTYPE html>
<html>
    <title>AccSys</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.maskedinput.js" ></script>
    
    <style>
        html,body,h1,h2,h3,h4,h5 {font-family: "Raleway", sans-serif}
    </style>

<body class="w3-light-grey">

    <!-- Top container -->
    <div class="w3-container w3-top w3-black w3-large w3-padding-16 w3-text-blue" style="z-index:4;">
        <span class="w3-left w3-hide-small w3-hide-medium"><?php echo $this->session->user_data->company; ?></span>
        <button class="w3-btn w3-hide-large w3-padding-0 w3-hover-text-grey" onclick="w3_open();"><i class="fa fa-bars"></i>  Menu</button>
        <span class="w3-right">Logo</span>
    </div>

    <?php $this->load->view($sidenav);?>
    <?php $this->load->view($content);?>
  
    <script>
        // Get the Sidenav
        var mySidenav = document.getElementById("mySidenav");

        // Get the DIV with overlay effect
        var overlayBg = document.getElementById("myOverlay");

        // Toggle between showing and hiding the sidenav, and add overlay effect
        function w3_open() {
            if (mySidenav.style.display === 'block') {
                mySidenav.style.display = 'none';
                overlayBg.style.display = "none";
            } else {
                mySidenav.style.display = 'block';
                overlayBg.style.display = "block";
            }
        }

        // Close the sidenav with the close button
        function w3_close() {
            mySidenav.style.display = "none";
            overlayBg.style.display = "none";
        }
    </script>

</body>
</html>

