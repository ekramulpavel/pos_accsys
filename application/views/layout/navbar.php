    <!-- Sidenav/menu -->
    <nav class="w3-sidenav w3-collapse w3-grey" style="z-index:3;width:250px;" id="mySidenav"><br>
        <div class="w3-container w3-row">
            <div class="w3-col s10">
                <span>Welcome, <strong><?php echo $this->session->user_data->user_name;?></strong></span><br>
                <a href="<?php echo base_url();?>home" class="w3-hover-none w3-hover-text-teal w3-show-inline-block"><i class="fa fa-home"></i></a>
                <a href="#" class="w3-hover-none w3-hover-text-red w3-show-inline-block"><i class="fa fa-envelope"></i></a>
                <a href="#" class="w3-hover-none w3-hover-text-green w3-show-inline-block"><i class="fa fa-user"></i></a>
                <a href="#" class="w3-hover-none w3-hover-text-blue w3-show-inline-block"><i class="fa fa-cog"></i></a>
            </div>
        </div>
        <hr>
        
        <div class="w3-container">
            <h5><?php echo isset($title)? "$title": NULL;?></h5>
        </div>
        <a href="#" class="w3-padding-16 w3-hide-large w3-dark-grey w3-hover-black" onclick="w3_close()" title="close menu"><i class="fa fa-remove fa-fw"></i>  Close Menu</a>
        <a href="#" class="w3-padding w3-blue"><i class="fa fa-eye fa-fw"></i>  Overview</a>
        <?php 
            foreach($projects as $key =>$value) {
                echo "$value";
            }
        ?>
        <a href="<?php echo base_url(); ?>login/logout" class="w3-padding"><i class="fa fa-sign-out"></i>  Sign-out</a>
        <!--
        <div class="w3-accordion w3-padding">
            <a onclick="myAccFunc()" href="#">Accordion <i class="fa fa-caret-down"></i></a>
            <div id="demoAcc" class="w3-accordion-content w3-dark-grey">
                <a href="#"><i class="w3-padding"></i>Link 1</a>
                <a href="#"><i class="w3-padding"></i>Link 2</a>
                <a href="#"><i class="w3-padding"></i>Link 3</a>
            </div>
        </div>
        <a href="<?php echo base_url(); ?>inventory" class="w3-padding"><i class="fa fa-users fa-fw"></i>  Inventory</a>
        <a href="#" class="w3-padding"><i class="fa fa-diamond fa-fw"></i>  Orders</a>
        <a href="#" class="w3-padding"><i class="fa fa-bell fa-fw"></i>  News</a>
        <a href="#" class="w3-padding"><i class="fa fa-bank fa-fw"></i>  General</a>
        <a href="#" class="w3-padding"><i class="fa fa-history fa-fw"></i>  History</a>
        <a href="#" class="w3-padding"><i class="fa fa-cog fa-fw"></i>  Settings</a>
        -->
    </nav>


    <!-- Overlay effect when opening sidenav on small screens -->
    <div class="w3-overlay w3-hide-large w3-animate-opacity" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>
