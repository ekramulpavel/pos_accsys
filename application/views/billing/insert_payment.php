<!-- !PAGE CONTENT! -->
    <div class="w3-main" style="margin-left:250px;margin-top:43px;">

        <!-- Header -->
        <header class="w3-container" style="padding-top:22px">
            <h5><b><i class="fa fa-dashboard"></i><?php echo isset($title)? " AccSys - $title": NULL;?></b></h5>
        </header>

        <br>
     <div class="w3-container" >
        <div class="w3-panel w3-pale-teal w3-bottombar w3-border-teal w3-border">
            <div class="w3-container w3-teal ">
                <h4 class="w3-left"><?php echo $form_title;?><span id="total_client"></span></h4>
                <h6 class="w3-right"><?php echo isset($msg)? "$msg": NULL;?></h6>
            </div>
            <div class="w3-row-padding">
            <br>
                <div class="w3-col s12 m12 l12">
                    <label>Client ID</label>
                    <input class="w3-input w3-border w3-round w3-hover-sand param" type="text" id="client_id">
                </div>
            </div>
            <form class="w3-container" action="<?php echo base_url();?>billing/insert_client_payment" method="post">
                <br>
                <div class="w3-row-padding">
                    <div class="w3-col s12 m12 l2">
                        <label>Client ID</label>
                        <input type="hidden" name="company_id" id="table" value="<?php echo $this->session->user_data->company_id;?>">
                        <input type="hidden" name="table" id="table" value="<?php echo $table;?>">
                        <input class="w3-input w3-border w3-round w3-grey" type="text" id="id" name="client_id" readonly>
                    </div>
                    <div class="w3-col s12 m12 l4">
                        <label>Client Name</label>
                        <input class="w3-input w3-border w3-round w3-grey" type="text" id="client_name" readonly>
                    </div>
                    <div class="w3-col s12 m12 l2">
                        <label>Package</label>
                        <input class="w3-input w3-border w3-round w3-grey" type="text" id="pkg_name" readonly>
                    </div>
                    <div class="w3-col s12 m12 l2">
                        <label>Payment Date</label>
                        <input class="w3-input w3-border w3-round w3-grey" type="hidden" id="due_month" name="due_month" readonly>
                        <input class="w3-input w3-border w3-round w3-hover-sand" type="date" id="date" name="date" maxlength="8" required>
                    </div>
                    <div class="w3-col s12 m12 l2 w3-right">
                        <label>Amount Paid</label>
                        <input class="w3-input w3-border w3-round w3-hover-sand" type="text" id="amount_paid" name="amount_paid" required>
                    </div>
                </div>
                <br>
                <div class="w3-row-padding">
                    <div class="w3-col s12 m12 l12">
                        <button class="w3-btn w3-teal w3-left" id="back" type="button">Cancel</button> 
                        <button class="w3-btn w3-teal w3-right" type="submit">Submit</button>
                    </div>
                </div>
                <br>
            </form>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#client_id").focus();
            $("#client_id").change(function(){
                $.ajax({
                    url: "<?php echo base_url(); ?>billing/check_client_validity",
                    data: {client_id: $(this).val()},
                    type: "POST",
                    dataType:"json",
                    success: function (data) {
                            $("#id").val(data[0].client_id);
                            $("#client_name").val(data[0].client_name);
                            $("#pkg_name").val(data[0].pkg_name);
                            $("#due_month").val(data[1].due_month);
                            //$("#amount_paid").val(data[1].amount_due);
                            //alert(data);
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                        alert("You entered an invalid Client ID  !!!");
                        $("#client_id").focus().select();
                    }
                });
                $("#date").focus();
            });

            $("#back").click(function(){
                history.back();
            })
        });
    </script>