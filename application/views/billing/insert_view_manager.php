    <!-- !PAGE CONTENT! -->
    <div class="w3-main" style="margin-left:250px;margin-top:43px;">

        <!-- Header -->
        <header class="w3-container" style="padding-top:22px">
            <h5><b><i class="fa fa-dashboard"></i><?php echo isset($title)? " AccSys - $title": NULL;?></b></h5>
        </header>

        <br>
     <div class="w3-container" >
        <div class="w3-panel w3-pale-teal w3-bottombar w3-border-teal w3-border">
            <div class="w3-container w3-teal ">
                <h4 class="w3-left"><?php echo $form_title.': Total - ';?><span id="total_client"><?php echo count((array)$data)?></span></h4>
                <h6 class="w3-right"><?php echo isset($msg)? "$msg": NULL;?></h6>
            </div>
            <form class="w3-container" action="<?php echo base_url();?>billing/insert_all" method="post">
            <br>
                <div class="w3-row-padding">
                    <div class="w3-col s12 m6 l2">
                        <label>Manager ID</label>
                        <input type="hidden" name="table" id="table" value="<?php echo $table;?>">
                        <input type="hidden" name="company_id" value="<?php echo $this->session->user_data->company_id; ?>" />
                        <input class="w3-input w3-border w3-round w3-hover-sand param" type="text" id="str_src" value="<?php echo isset($max->id)? $max->id+1: $max->id=1;?>" name="managed_id" readonly>
                    </div>
                    <div class="w3-col s12 m6 l10">
                        <label>Manager</label>
                        <input class="w3-input w3-border w3-round w3-hover-sand param" type="text" id="str_src" value="" name="managed_by" required>
                    </div>
                </div>
                <br>
                <div class="w3-row-padding">
                    <div class="w3-col s12 m12 l12">
                        <button class="w3-btn w3-teal w3-left" id="back" type="button">Cancel</button> 
                        <button class="w3-btn w3-teal w3-right" type="submit">Submit</button>
                    </div>
                </div>
                <br>
            </form>
        </div>
        <div class="w3-row-padding">
            <div class="w3-col s12 m12 l12">
                <label>String Search</label>
                <input class="w3-input w3-border w3-round w3-hover-sand param" type="text" id="str_src">
            </div>
        </div>
        <br>
        <div class="w3-responsive" id="existing-data">
            <table class="w3-table-all">
                <thead>
                    <tr class="w3-teal">
                        <th>Manager ID</th>
                        <th>Manager</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                    foreach ($data as $key => $value) {
                ?>
                    <tr>
                        <td><a href=""><?php echo $value->managed_id ?></a></td>
                        <td><?php echo $value->managed_by ?></td>
                        <td>
                            <a href="<?php echo base_url().'billing/edit_client/'.$table.'/'.$value->managed_id; ?>" class="fa fa-edit" style="text-decoration:none;"></a> &nbsp;|&nbsp;
                            <a href="<?php echo base_url().'billing/del/'.$table.'/'.$value->managed_id; ?>" class="fa fa-trash-o delete" style="text-decoration:none;"></a>
                        </td>
                    
                    </tr>
                <?php       
                    }
                ?>
                </tbody>
            </table>
        </div>

        <div class="w3-responsive" id="param-data">
           
        </div>

    </div>
    </div>