    <!-- !PAGE CONTENT! -->
    <div class="w3-main" style="margin-left:250px;margin-top:43px;">

        <!-- Header -->
        <header class="w3-container" style="padding-top:22px">
            <h5><b><i class="fa fa-dashboard"></i><?php echo isset($title)? " AccSys - $title": NULL;?></b></h5>
        </header>

        <br>
     <div class="w3-container" >
        <div class="w3-panel w3-pale-teal w3-bottombar w3-border-teal w3-border">
            <div class="w3-container w3-teal ">
                <h4 class="w3-left"><?php echo $form_title.': Total Client-';?><span id="total_client"><?php echo count((array)$client_list)?></span></h4>
                <h6 class="w3-right"><?php echo isset($msg)? "$msg": NULL;?></h6>
            </div>
            <!--<form class="w3-container" action="" method="post">-->
            <br>
                <div class="w3-row-padding">
                    <div class="w3-col s12 m12 l12">
                        <label>String Search</label>
                        <input type="hidden" name="table" id="table" value="<?php echo $table;?>">
                        <input class="w3-input w3-border w3-round w3-hover-sand param" type="text" id="str_src" value="" name="search_string">
                    </div>
                </div>
                <br>
                <div class="w3-row-padding">
                    <div class="w3-col s12 m6 l2">
                        <label>Client ID</label>
                        <select class="w3-input w3-border w3-round w3-hover-sand param" id="client_id" name="client_id">
                                <?php print_r($client);?>
                        </select>
                    </div>
                    <div class="w3-col s12 m6 l2">
                        <label>Connection Type</label>
                        <select class="w3-input w3-border w3-round w3-hover-sand param" id="conn_type" name="conn_type">
                                <?php print_r($conn_type);?>
                        </select>
                    </div>
                    <div class="w3-col s12 m6 l2">
                        <label>Package</label>
                        <select class="w3-input w3-border w3-round w3-hover-sand param" id="pkg_id" name="pkg_id">
                                <?php print_r($pkg);?>
                        </select>
                    </div>
                    <div class="w3-col s12 m6 l2">
                        <label>Area</label>
                        <select class="w3-input w3-border w3-round w3-hover-sand param" id="area_id" name="area_id">
                                <?php print_r($area);?>
                        </select>
                    </div>
                    <div class="w3-col s12 m6 l2">
                        <label>Managed By</label>
                        <select class="w3-input w3-border w3-round w3-hover-sand param" id="managed_by" name="managed_by">
                                <?php print_r($managed_by);?>
                        </select>
                    </div>
                    <div class="w3-col s12 m6 l2">
                        <label>Status</label>
                        <select class="w3-input w3-border w3-round w3-hover-sand param" id="status" name="status">
                                <?php print_r($status);?>
                        </select>
                    </div>
                </div>
                <br>
            <!--</form>-->
        </div>

        <div class="w3-responsive" id="existing-data">
            <table class="w3-table-all w3-small">
                <thead>
                    <tr class="w3-teal">
                        <th>Client ID</th>
                        <th>Client Name</th>
                        <th>IP/Card #</th>
                        <th>Connection Date</th>
                        <th>Mobile</th>
                        <th>Package</th>
                        <th>Connection Type</th>
                        <th>Status</th>
                        <th>Area</th>
                        <th>Managed By</th>
                        <th>Remarks</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                    foreach ($client_list as $key => $value) {
                ?>
                    <tr>
                        <td><a href=""><?php echo $value->client_id ?></a></td>
                        <td><?php echo $value->client_name ?></td>
                        <td><?php echo $value->ip_card ?></td>
                        <td><?php echo $value->con_date ?></td>
                        <td><?php echo $value->mobile ?></td>
                        <td><?php echo $value->pkg_name ?></td>
                        <td><?php echo $value->conn_type ?></td>
                        <td><?php echo $value->status ?></td>
                        <td><?php echo $value->area ?></td>
                        <td><?php echo $value->managed_by ?></td>
                        <td><?php echo $value->remarks ?></td>
                        <td>
                            <a href="<?php echo base_url().'billing/edit_client/'.$table.'/'.$value->client_id; ?>" class="fa fa-edit" style="text-decoration:none;"></a> &nbsp;|&nbsp;
                            <a href="<?php echo base_url().'billing/del/'.$table.'/'.$value->client_id; ?>" class="fa fa-trash-o delete" style="text-decoration:none;"></a>
                        </td>
                    
                    </tr>
                <?php       
                    }
                ?>
                </tbody>
            </table>
        </div>

        <div class="w3-responsive" id="param-data">
           
        </div>

    </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#str_src").focus();
            $(".param").change(function () {
                
                var dataString = {
                    str_src: $('#str_src').val(),
                    client_id: $('#client_id').val(),
                    conn_type: $('#conn_type').val(),
                    pkg_id: $('#pkg_id').val(),
                    area_id: $('#area_id').val(),
                    managed_by: $('#managed_by').val(),
                    status: $('#status').val()
                };
                var jsonString = JSON.stringify(dataString);

                $.ajax({
                    url: "<?php echo base_url(); ?>billing/viewClientByParam",
                    data: {data: jsonString},
                    type: "POST",
                    dataType: 'json',
                    success: function (data) {
                        tabledata(data);
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                        alert("some error");
                    }
                });

            });

            function tabledata(data){
                $('#existing-data').hide();
                var table_name=$('#table').val();

                var html =  '<table class="w3-table-all w3-small" id="param-data"><thead><tr class="w3-teal">'+
                            '<th>Client ID</th>'+
                            '<th>Client Name</th>'+
                            '<th>IP/Card #</th>'+
                            '<th>Connection Date</th>'+
                            '<th>Mobile</th>'+
                            '<th>Package</th>'+
                            '<th>Connection Type</th>'+
                            '<th>Status</th>'+
                            '<th>Area</th>'+
                            '<th>Managed By</th>'+
                            '<th>Remarks</th>'+
                            '<th>Action</th>'+
                            '</tr></thead><tbody>';
                        
                $.each(data, function(index, item) {
                    html +='<tr>';
                    html +='<td><a href="#">'+ item.client_id + '</a></td>';
                    html +='<td>'+ item.client_name + '</td>';
                    html +='<td>'+ item.ip_card + '</td>';
                    html +='<td>'+ item.con_date + '</td>';
                    html +='<td>'+ item.mobile + '</td>';
                    html +='<td>'+ item.pkg_name + '</td>';
                    html +='<td>'+ item.conn_type + '</td>';
                    html +='<td>'+ item.status + '</td>';
                    html +='<td>'+ item.area + '</td>';
                    html +='<td>'+ item.managed_by + '</td>';
                    html +='<td>'+ item.remarks + '</td>';
                    html +='<td>'+ '<a href="<?php echo base_url().'billing/edit_client/';?>'+table_name+'/'+item.client_id+'" class="fa fa-edit" style="text-decoration:none;"></a> &nbsp;|&nbsp;'+
                                   '<a href="<?php echo base_url().'billing/del/';?>'+table_name+'/'+item.client_id+'" class="fa fa-trash-o delete" style="text-decoration:none;"></a>';
                    html +='</td></tr>';
                });

                html += '</tbody></table>';
                $('#total_client').text(data.length);
                $('#param-data').html(html);
            }

           
            $(document).on('click','.delete', function(e){
                if(!confirm('Are you sure?')){
                    e.preventDefault();
                    return false;
                }
                return true;
            });
        });
    </script>