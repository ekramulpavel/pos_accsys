    <!-- !PAGE CONTENT! -->
    <div class="w3-main" style="margin-left:250px;margin-top:43px;">

        <!-- Header -->
        <header class="w3-container" style="padding-top:22px">
            <h5><b><i class="fa fa-dashboard"></i><?php echo isset($title)? " AccSys - $title": NULL;?></b></h5>
        </header>

        <div class="w3-row-padding w3-margin-bottom">
            <div class="w3-quarter">
                <div class="w3-container w3-red w3-padding-16">
                    <div class="w3-left"><i class="fa fa-users w3-xxxlarge"></i></div>
                    <div class="w3-right">
                        <h3><?php echo $this->db->count_all('bill_client');?></h3>
                    </div>
                    <div class="w3-clear"></div>
                    <h4>Total Client</h4>
            </div>
            </div>
            <div class="w3-quarter">
                <div class="w3-container w3-blue w3-padding-16">
                    <div class="w3-left"><i class="fa fa-rss w3-xxxlarge"></i></div>
                    <div class="w3-right">
                        <h3><?php echo $this->db->where('status',1)->from('bill_client')->count_all_results();?></h3>
                    </div>
                    <div class="w3-clear"></div>
                    <h4>Active Client</h4>
                </div>
            </div>
            <div class="w3-quarter">
                <div class="w3-container w3-teal w3-padding-16">
                    <div class="w3-left"><i class="fa fa-times w3-xxxlarge"></i></div>
                    <div class="w3-right">
                        <h3><?php echo $this->db->where('status',2)->from('bill_client')->count_all_results();?></h3>
                    </div>
                    <div class="w3-clear"></div>
                    <h4>Inactive Client</h4>
                </div>
            </div>
            <div class="w3-quarter">
                <div class="w3-container w3-orange w3-text-white w3-padding-16">
                    <div class="w3-left"><i class="fa fa-pause-circle-o w3-xxxlarge"></i></div>
                    <div class="w3-right">
                        <h3><?php echo $this->db->where('status',3)->from('bill_client')->count_all_results();?></h3>
                    </div>
                    <div class="w3-clear"></div>
                    <h4>Client Paused</h4>
                </div>
            </div>
        </div>

      <div class="w3-container w3-section">
            <div class="w3-row-padding" style="margin:0 -16px">
                <div class="w3-half">
                    <h5>Total Client By Package</h5>
                    <table class="w3-table w3-striped w3-white">
                      <thead><tr class="w3-grey">
                            <th><i class="w3-blue w3-padding-tiny"></i></th>
                            <th>Pakcage</th>
                            <th>Rate</th>
                            <th># of Client</th>
                        </thead></tr>  
                        <tbody>
                    <?php
                        foreach($pkg_list as $key=>$value){
                    ?>
                        <tr class="w3-padding-0">
                            <td><i class="w3-blue w3-padding-tiny"></i></td>
                            <td><?php echo $value->pkg_name;?></td>
                            <td><?php echo $value->pkg_rate;?></td>
                            <td><i><?php echo $value->Total_Client;?></i></td>
                        </tr>
                    <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="w3-half">
                    <h5>Total Client By Area</h5>
                    <table class="w3-table w3-striped w3-white">
                      <thead><tr class="w3-grey">
                            <th><i class="w3-teal w3-padding-tiny"></i></th>
                            <th>#</th>
                            <th>Area</th>
                            <th># of Client</th>
                        </thead></tr>  
                        <tbody>
                    <?php
                        foreach($area_list as $key=>$value){
                    ?>
                        <tr>
                            <td><i class="w3-teal w3-padding-tiny"></i></td>
                            <td><?php echo $value->area_id;?></td>
                            <td><?php echo $value->area;?></td>
                            <td><i><?php echo $value->Total_Client;?></i></td>
                        </tr>
                    <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <hr>
        <div class="w3-container">
            <h5>General Stats</h5>
            <p>New Visitors</p>
            <div class="w3-progress-container w3-grey">
                <div id="myBar" class="w3-progressbar w3-green" style="width:25%">
                    <div class="w3-center w3-text-white">+25%</div>
                </div>
            </div>

            <p>New Users</p>
            <div class="w3-progress-container w3-grey">
                <div id="myBar" class="w3-progressbar w3-orange" style="width:50%">
                    <div class="w3-center w3-text-white">50%</div>
                </div>
            </div>

            <p>Bounce Rate</p>
            <div class="w3-progress-container w3-grey">
                <div id="myBar" class="w3-progressbar w3-red" style="width:75%">
                    <div class="w3-center w3-text-white">75%</div>
                </div>
            </div>
        </div>
        <hr>

        <div class="w3-container">
            <h5>Countries</h5>
            <table class="w3-table w3-striped w3-bordered w3-border w3-hoverable w3-white">
                <tr>
                    <td>United States</td>
                    <td>65%</td>
                </tr>
                <tr>
                    <td>UK</td>
                    <td>15.7%</td>
                </tr>
                <tr>
                    <td>Russia</td>
                    <td>5.6%</td>
                </tr>
                <tr>
                    <td>Spain</td>
                    <td>2.1%</td>
                </tr>
                <tr>
                    <td>India</td>
                    <td>1.9%</td>
                </tr>
                <tr>
                    <td>France</td>
                    <td>1.5%</td>
                </tr>
            </table><br>
            <button class="w3-btn">More Countries  <i class="fa fa-arrow-right"></i></button>
        </div>
        <hr>
        
        <div class="w3-container w3-dark-grey w3-padding-32">
            <div class="w3-row">
                <div class="w3-container w3-third">
                    <h5 class="w3-bottombar w3-border-green">Demographic</h5>
                    <p>Language</p>
                    <p>Country</p>
                    <p>City</p>
                </div>
                <div class="w3-container w3-third">
                    <h5 class="w3-bottombar w3-border-red">System</h5>
                    <p>Browser</p>
                    <p>OS</p>
                    <p>More</p>
                </div>
                <div class="w3-container w3-third">
                    <h5 class="w3-bottombar w3-border-orange">Target</h5>
                    <p>Users</p>
                    <p>Active</p>
                    <p>Geo</p>
                    <p>Interests</p>
                </div>
            </div>
      </div>

        <!-- Footer -->
        <footer class="w3-container w3-light-grey w3-text-blue">
            <p>Powered by <a href="http://vividwork.com" target="_blank">vividwork.com</a></p>
        </footer>

      <!-- End page content -->
    </div>