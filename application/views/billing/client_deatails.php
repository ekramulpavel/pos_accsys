    <!-- !PAGE CONTENT! -->
    <div class="w3-main" style="margin-left:250px;margin-top:43px;">

        <!-- Header -->
        <header class="w3-container" style="padding-top:22px">
            <h5><b><i class="fa fa-dashboard"></i><?php echo isset($title)? " AccSys - $title": NULL;?></b></h5>
        </header>

        <br>
     <div class="w3-container" >
        <div class="w3-panel w3-pale-teal w3-bottombar w3-border-teal w3-border">
            <div class="w3-container w3-teal ">
                <h4 class="w3-left"><?php echo $form_title.': Total - ';?><span id="total_client"><?php //echo count((array)$data)?></span></h4>
                <h6 class="w3-right"><?php echo isset($msg)? "$msg": NULL;?></h6>
            </div>
            <!--<form class="w3-container" action="<?php echo base_url();?>billing/insert_all" method="post">-->
            <br>
                <div class="w3-row-padding">
                    <div class="w3-col s12 m6 l8">
                        <label>Client ID</label>
                        <input class="w3-input w3-border w3-round w3-hover-sand param" type="text" id="client_id" required>
                    </div>
                    <div class="w3-col s12 m6 l2">
                        <label>From Date</label>
                        <input class="w3-input w3-border w3-round w3-hover-sand param" type="date" id="fromDate">
                    </div>
                    <div class="w3-col s12 m6 l2">
                        <label>To Date</label>
                        <input class="w3-input w3-border w3-round w3-hover-sand param" type="date" id="toDate">
                    </div>
                </div>
                <br>
                <div class="w3-row-padding">
                    <div class="w3-col s12 m12 l12">
                        <button class="w3-btn w3-teal w3-left" id="back" type="button">Cancel</button> 
                        <button class="w3-btn w3-teal w3-right" id="submit" type="submit">Submit</button>
                    </div>
                </div>
                <br>
            <!--</form>-->
        </div>

        <div class="w3-responsive" id="param-data">

            <?php echo isset($data)?$data: null?>
            
            <?php if (!empty($jdata)): ?>
                <table>
                    <thead>
                    <tr class="w3-teal">
                        <th>Date</th>
                        <th>Description</th>
                        <th>Debit</th>
                        <th>Credit</th>
                        <th>Balance</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <?php foreach ($data as $key => $value): ?>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <?php endforeach ?>
                    </tr>
                    </tbody>
                </table>
            <?php endif ?>
            
        </div>

    </div>
    </div>
     <script type="text/javascript">
        $(document).ready(function () {
            $("#client_id").focus();
            $(".param").change(function () {
                var dataString = {
                    client_id: $('#client_id').val(),
                    from: $('#fromDate').val(),
                    to: $('#toDate').val()
                };
                var jsonString = JSON.stringify(dataString);

                $.ajax({
                    url: "<?php echo base_url(); ?>billing/client_deatails",
                    data: {data: jsonString},
                    type: "POST",
                    //dataType: 'json',
                    success: function (data) {
                        $('#param-data').html(data);
                        //alert(data);
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                        alert("some error");
                    }
                });
            });
        });
    </script>