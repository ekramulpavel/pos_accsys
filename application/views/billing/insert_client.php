    <!-- !PAGE CONTENT! -->
    <div class="w3-main" style="margin-left:250px;margin-top:43px;">

        <!-- Header -->
        <header class="w3-container" style="padding-top:22px">
            <h5><b><i class="fa fa-dashboard"></i><?php echo isset($title)? " AccSys - $title": NULL;?></b></h5>
        </header>

        <br>
    <div class="w3-container">
        <div class="w3-panel w3-pale-teal w3-bottombar w3-border-teal w3-border">
            <div class="w3-container w3-teal">
                <h4 class="w3-left"><?php echo $form_title;?></h4>
                <h6 class="w3-right"><?php echo isset($msg)? "$msg": NULL;?></h6>
            </div>
            <form class="w3-container" action="<?php echo base_url();?>billing/insert" method="post" enctype="multipart/form-data">
            <br>
                <div class="w3-row-padding">
                    <div class="w3-col s12 m12 l3">
                        <label>Client ID</label>
                        <input type="hidden" name="table" value="<?php echo $table;?>">
                        <input class="w3-input w3-border w3-round w3-hover-sand" type="text" id="client_id" name="client_id" required>
                    </div>
                    <div class="w3-col s12 m12 l3">
                        <label>IP or Card #</label>
                        <input class="w3-input w3-border w3-round w3-hover-sand" type="text" id="" value="" name="ip_card" required>
                    </div>
                    <div class="w3-col s12 m12 l6">
                        <label>Client Name</label>
                        <input class="w3-input w3-border w3-round w3-hover-sand" type="text" id="" value="" name="client_name" required>
                    </div>
                </div>
                <br>
                <div class="w3-row-padding">
                    <div class="w3-col s12 m3 l3">
                        <label>National ID Card #</label>
                        <input class="w3-input w3-border w3-round w3-hover-sand" type="text" id="nid" name="nid">
                    </div>
                    <div class="w3-col s12 m9 l9">
                        <label>Address</label>
                        <input class="w3-input w3-border w3-round w3-hover-sand" type="text" name="address" required>
                    </div>
                </div>
                <br>
                <div class="w3-row-padding">
                    <div class="w3-col s12 m12 l3">
                        <label>Opening Balance</label>
                        <input class="w3-input w3-border w3-round w3-hover-sand" type="number" name="opening_balance" required>
                    </div>
                    <div class="w3-col s12 m12 l3">
                        <label>Connection Charge</label>
                        <input class="w3-input w3-border w3-round w3-hover-sand" type="number" name="conn_charge" required>
                    </div>
                    <div class="w3-col s12 m12 l3">
                        <label>Connection Date</label>
                        <input class="w3-input w3-border w3-round w3-hover-sand" type="date" name="con_date" required>
                    </div>
                    <div class="w3-col s12 m12 l3">
                        <label>Connection Type</label>
                        <select class="w3-input w3-border w3-round w3-hover-sand" name="conn_type" required>
                                <?php print_r($conn_type);?>
                        </select>
                    </div>                    
                </div>
                <br>
                <div class="w3-row-padding">
                    <div class="w3-col s12 m12 l3">
                        <label>Package</label>
                        <select class="w3-input w3-border w3-round w3-hover-sand" name="pkg_id" required>
                                <?php print_r($pkg);?>
                        </select>
                    </div>
                    <div class="w3-col s12 m12 l3">
                        <label>Area</label>
                        <select class="w3-input w3-border w3-round w3-hover-sand" name="area_id" required>
                                <?php print_r($area);?>
                        </select>
                    </div>
                    <div class="w3-col s12 m12 l3">
                        <label>Managed By</label>
                        <select class="w3-input w3-border w3-round w3-hover-sand" name="managed_by" required>
                                <?php print_r($managed_by);?>
                        </select>
                    </div>
                    <div class="w3-col s12 m12 l3">
                        <label>Status</label>
                        <select class="w3-input w3-border w3-round w3-hover-sand" name="status" required>
                                <?php print_r($status);?>
                        </select>
                    </div>
                </div>
                <br>
                <div class="w3-row-padding">
                    <div class="w3-col s12 m12 l2">
                        <label>Mobile</label>
                        <input class="w3-input w3-border w3-round w3-hover-sand" type="text" id="mobile" name="mobile" required>
                    </div>
                    <div class="w3-col s12 m12 l8">
                        <label>Remarks</label>
                        <input class="w3-input w3-border w3-round w3-hover-sand" type="text" name="remarks">
                    </div>
                    <div class="w3-col s12 m12 l2">
                        <label>Client Image</label><br>
                        <input type="file" name="userfile" accept="image/*">
                    </div>
                </div>
                <br>
                <div class="w3-row-padding">
                    <div class="w3-col s12 m12 l12">
                        <button class="w3-btn w3-teal w3-left" id="back" type="button">Cancel</button> 
                        <button class="w3-btn w3-teal w3-right" type="submit">Submit</button>
                    </div>
                </div>
                <br>
            </form>
        </div>
    </div>
    </div>
    
    <script type="text/javascript">
        $(document).ready(function () {
            $("#client_id").focus();
            $("#client_id").change(function () {
                $.ajax({
                    url: "<?php echo base_url(); ?>billing/chk_duplicate_client_id",
                    data: {client_id: $(this).val()},
                    type: "POST",
                    success: function (data) {
                        if(data!=0){
                            alert(data);
                        }
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                        alert("some error");
                    }
                });

            });
            $("#mobile").mask("99999-999999");
            $("#nid").mask("9999999999999");
            $("#back").click(function(){
                history.back();
            })
        });
    </script>