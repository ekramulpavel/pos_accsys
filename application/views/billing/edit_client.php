    <!-- !PAGE CONTENT! -->
    <div class="w3-main" style="margin-left:250px;margin-top:43px;">

        <!-- Header -->
        <header class="w3-container" style="padding-top:22px">
            <h5><b><i class="fa fa-dashboard"></i><?php echo isset($title)? " AccSys - $title": NULL;?></b></h5>
        </header>

        <br>
    <div class="w3-container">
        <div class="w3-panel w3-pale-teal w3-bottombar w3-border-teal w3-border">
            <div class="w3-container w3-teal">
                <h4 class="w3-left"><?php echo $form_title;?></h4>
                <h6 class="w3-right"><?php echo isset($msg)? "$msg": NULL;?></h6>
            </div>
            <form class="w3-container" action="<?php echo base_url();?>billing/update_client" method="post" enctype="multipart/form-data">
            <br>
                <div class="w3-row-padding">
                    <div class="w3-col s12 m12 l3">
                        <label>Client ID</label>
                        <input type="hidden" name="table" value="<?php echo $table;?>">
                        <input class="w3-input w3-border w3-round w3-hover-sand" type="text" name="client_id" value="<?php echo $client_list[0]->client_id;?>" readonly>
                    </div>
                    <div class="w3-col s12 m12 l3">
                        <label>IP Oo Card #</label>
                        <input class="w3-input w3-border w3-round w3-hover-sand" type="text" id="" name="ip_card" value="<?php echo $client_list[0]->ip_card;?>" required>
                    </div>
                    <div class="w3-col s12 m12 l6">
                        <label>Client Name</label>
                        <input class="w3-input w3-border w3-round w3-hover-sand" type="text" id="" name="client_name" value="<?php echo $client_list[0]->client_name;?>" required>
                    </div>
                </div>
                <br>
                <div class="w3-row-padding">
                    <div class="w3-col s12 m3 l3">
                        <label>National ID Card #</label>
                        <input class="w3-input w3-border w3-round w3-hover-sand" type="text" value="<?php echo $client_list[0]->nid;?>" id="nid" name="nid" >
                    </div>
                    <div class="w3-col s12 m9 l9">
                        <label>Address</label>
                        <input class="w3-input w3-border w3-round w3-hover-sand" type="text" name="address" value="<?php echo $client_list[0]->address;?>" required>
                    </div>
                </div>
                <br>
                <div class="w3-row-padding">
                    <div class="w3-col s12 m12 l3">
                        <label>Opening Balance</label>
                        <input class="w3-input w3-border w3-round w3-hover-sand" type="number" name="opening_balance" value="<?php echo $client_list[0]->opening_balance;?>" required>
                    </div>
                    <div class="w3-col s12 m12 l3">
                        <label>Connection Charge</label>
                        <input class="w3-input w3-border w3-round w3-hover-sand" type="number" name="conn_charge" value="<?php echo $client_list[0]->conn_charge;?>" required>
                    </div>
                    <div class="w3-col s12 m12 l3">
                        <label>Connection Date</label>
                        <input class="w3-input w3-border w3-round w3-hover-sand" type="date" name="con_date" value="<?php echo $client_list[0]->con_date;?>" required>
                    </div>
                    <div class="w3-col s12 m12 l3">
                        <label>Connection Type</label>
                        <select class="w3-input w3-border w3-round w3-hover-sand" name="conn_type" required>
                            <option value="<?php echo $client_list[0]->conn_id;?>" selected><?php echo $client_list[0]->conn_type;?></option>
                            <?php unset($conn_type[0]); print_r($conn_type);?>
                        </select>
                    </div>                    
                </div>
                <br>
                <div class="w3-row-padding">
                    <div class="w3-col s12 m12 l3">
                        <label>Package</label>
                        <select class="w3-input w3-border w3-round w3-hover-sand" name="pkg_id" required>
                            <option value="<?php echo $client_list[0]->pkg_id;?>" selected><?php echo $client_list[0]->pkg_name;?></option>
                            <?php unset($pkg[0]); print_r($pkg);?>
                        </select>
                    </div>
                    <div class="w3-col s12 m12 l3">
                        <label>Area</label>
                        <select class="w3-input w3-border w3-round w3-hover-sand" name="area_id" required>
                            <option value="<?php echo $client_list[0]->area_id;?>" selected><?php echo $client_list[0]->area;?></option>
                            <?php unset($area[0]); print_r($area);?>
                        </select>
                    </div>
                    <div class="w3-col s12 m12 l3">
                        <label>Managed By</label>
                        <select class="w3-input w3-border w3-round w3-hover-sand" name="managed_by" required>
                            <option value="<?php echo $client_list[0]->managed_id;?>" selected><?php echo $client_list[0]->managed_by;?></option>
                            <?php unset($managed_by[0]); print_r($managed_by);?>
                        </select>
                    </div>
                    <div class="w3-col s12 m12 l3">
                        <label>Status</label>
                        <select class="w3-input w3-border w3-round w3-hover-sand" name="status" required>
                            <option value="<?php echo $client_list[0]->id;?>" selected><?php echo $client_list[0]->status;?></option>
                            <?php unset($status[0]); print_r($status);?>
                        </select>
                    </div>
                </div>
                <br>
                <div class="w3-row-padding">
                    <div class="w3-col s12 m12 l2">
                        <label>Mobile</label>
                        <input class="w3-input w3-border w3-round w3-hover-sand" type="text" id="mobile" name="mobile" value="<?php echo $client_list[0]->mobile;?>" required>
                    </div>
                    <div class="w3-col s12 m12 l8">
                        <label>Remarks</label>
                        <input class="w3-input w3-border w3-round w3-hover-sand" type="text" name="remarks" value="<?php echo $client_list[0]->remarks;?>">
                    </div>
                    <div class="w3-col s12 m12 l2">
                        <label>Client Image</label><br>
                         <input type="file" name="userfile" accept="image/*">
                    </div>
                </div>
                <br>
                <div class="w3-row-padding">
                    <div class="w3-col s12 m12 l12">
                        <div>
                            <button class="w3-btn w3-teal w3-left" id="back" type="button">Cancel</button> 
                            <button class="w3-btn w3-teal w3-right" type="submit">Update</button>
                        </div>
                    </div>
                </div>
                <br>
            </form>
        </div>
    </div>
    </div>
    
    <script type="text/javascript">
        $(document).ready(function () {
            $("#mobile").mask("99999-999999");
            $("#nid").mask("9999999999999");
            $("#back").click(function(){
                history.back();
            })
        });
    </script>