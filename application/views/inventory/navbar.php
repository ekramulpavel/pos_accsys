<style>
/* reset css */
    nav, ol, ul, li{
      margin: 0;
      padding: 0;
      border: 0;
      font-size: 100%;
      font: inherit;
      vertical-align: baseline;      
    }

    .nav a, .nav label {
      display: block;
      padding: .85rem;
      color: #fff;
      background-color: #9E9E9E;
      box-shadow: inset 0 -1px #F1F1F1;
      -webkit-transition: all .25s ease-in;
      transition: all .25s ease-in;
    }

    .nav a:focus, .nav a:hover, .nav label:focus, .nav label:hover {
      color: rgba(255, 255, 255, 0.5);
      background: #2196F3;
    }

    .nav label { cursor: pointer; }

    /* Styling first level lists items */
    .group-list a, .group-list label {
      padding-left: 2rem;
      background: #252525;
      box-shadow: inset 0 -1px #373737;
    }

    .group-list a:focus, .group-list a:hover, .group-list label:focus, .group-list label:hover { background: #131313; }

    /* Styling second level list items */
    .sub-group-list a, .sub-group-list label {
      padding-left: 3rem;
      background: #353535;
      box-shadow: inset 0 -1px #474747;
    }

    .sub-group-list a:focus, .sub-group-list a:hover, .sub-group-list label:focus, .sub-group-list label:hover { background: #232323; }

    /*Styling third level list items */
    .sub-sub-group-list a, .sub-sub-group-list label {
      padding-left: 4rem;
      background: #454545;
      box-shadow: inset 0 -1px #575757;
    }

    .sub-sub-group-list a:focus, .sub-sub-group-list a:hover, .sub-sub-group-list label:focus, .sub-sub-group-list label:hover { background: #333333; }

    /* Hide nested lists */
    .group-list, .sub-group-list, .sub-sub-group-list {
      height: 100%;
      max-height: 0;
      overflow: hidden;
      -webkit-transition: max-height .5s ease-in-out;
      transition: max-height .5s ease-in-out;
    }

    /* reset the height when checkbox is checked */
    .nav__list input[type=checkbox]:checked + label + ul { 
        max-height: 1000px; }

    /*Rotating chevron icon */
    label > span {
      float: right;
      -webkit-transition: -webkit-transform .65s ease;
      transition: transform .65s ease;
    }

    .nav__list input[type=checkbox]:checked + label > span {
      -webkit-transform: rotate(90deg);
      -ms-transform: rotate(90deg);
      transform: rotate(90deg);
    }
</style>

<!-- Sidenav/menu -->
<nav class="w3-sidenav w3-collapse w3-grey" style="z-index:3;width:250px;" id="mySidenav"><br>
    <div class="w3-container w3-row">
        <div class="w3-col s10">
            <span>Welcome, <strong><?php echo $this->session->user_data->user_name;?></strong></span><br>
            <a href="<?php echo base_url();?>home" class="w3-hover-none w3-hover-text-teal w3-show-inline-block"><i class="fa fa-home"></i></a>
            <a href="#" class="w3-hover-none w3-hover-text-red w3-show-inline-block"><i class="fa fa-envelope"></i></a>
            <a href="#" class="w3-hover-none w3-hover-text-green w3-show-inline-block"><i class="fa fa-user"></i></a>
            <a href="#" class="w3-hover-none w3-hover-text-blue w3-show-inline-block"><i class="fa fa-cog"></i></a>
        </div>
    </div>
    <hr>  
    <div class="w3-container">
        <h5><?php echo isset($title)? "$title": NULL;?></h5>
    </div>  


    <nav class="nav" role="navigation">
        <ul class="nav__list">
            <li>
                <input id="group-1" type="checkbox" hidden />
                <label for="group-1"><span class="fa fa-angle-right"></span> Trade</label>
                <ul class="group-list">
                    <li><a href="<?php echo base_url();?>inventory/insert_trade"> Insert Trade</a></li>
                    <li><a href="<?php echo base_url();?>inventory/view_trade"> View Trade</a></li>
                    <li>
                        <input id="sub-group-1" type="checkbox" hidden />
                        <label for="sub-group-1"><span class="fa fa-angle-right"></span> 2nd level</label>
                        <ul class="sub-group-list">
                            <li><a href="#">2nd level nav item</a></li>
                            <li><a href="#">2nd level nav item</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li>
                <input id="group-2" type="checkbox" hidden />
                <label for="group-2"><span class="fa fa-angle-right"></span> Brand</label>
                <ul class="group-list">
                    <li><a href="<?php echo base_url();?>inventory/insert_brand"> Insert Brand</a></li>
                    <li><a href="<?php echo base_url();?>inventory/view_brand"> View Brand</a></li>
                    <li>
                        <input id="sub-group-2" type="checkbox" hidden />
                        <label for="sub-group-2"><span class="fa fa-angle-right"></span> Inventory</label>
                        <ul class="sub-group-list">
                            <li><a href="#">2nd level nav item</a></li>
                            <li><a href="#">2nd level nav item</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li>
                <input id="group-3" type="checkbox" hidden />
                <label for="group-3"><span class="fa fa-angle-right"></span> Category</label>
                <ul class="group-list">
                    <li><a href="<?php echo base_url();?>inventory/insert_category"> Insert Category</a></li>
                     <li><a href="<?php echo base_url();?>inventory/view_category"> View Category</a></li>
                    <li>
                        <input id="sub-group-3" type="checkbox" hidden />
                        <label for="sub-group-3"><span class="fa fa-angle-right"></span> Second Level</label>
                        <ul class="sub-group-list">
                            <li><a href="#">2nd level nav item</a></li>
                            <li><a href="#">2nd level nav item</a></li>
                            <li><a href="#">2nd level nav item</a></li>
                            <li>
                                <input id="sub-sub-group-3" type="checkbox" hidden />
                                <label for="sub-sub-group-3"><span class="fa fa-angle-right"></span> Third level</label>
                                <ul class="sub-sub-group-list">
                                    <li><a href="#">3rd level nav item</a></li>
                                    <li><a href="#">3rd level nav item</a></li>
                                    <li><a href="#">3rd level nav item</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li>
                <input id="group-4" type="checkbox" hidden />
                <label for="group-4"><span class="fa fa-angle-right"></span> Products</label>
                <ul class="group-list">
                    <li><a href="<?php echo base_url();?>inventory/insert_products"> Insert Product</a></li>
                    <li><a href="<?php echo base_url();?>inventory/view_products"> View Product</a></li>
                    <li><a href="<?php echo base_url();?>inventory/product_details"> Product Details</a></li>
                    <li>
                        <input id="sub-group-4" type="checkbox" hidden />
                        <label for="sub-group-4"><span class="fa fa-angle-right"></span> Inventory</label>
                        <ul class="sub-group-list">
                            <li><a href="#"> Product Details</a></li>
                            <li><a href="#">Order History</a></li>
                            <li><a href="#">Purchase History</a></li>
                            <li><a href="#">Inventory</a></li>
                            <li>
                                <input id="sub-sub-group-4" type="checkbox" hidden />
                                <label for="sub-sub-group-4"><span class="fa fa-angle-right"></span> Third level</label>
                                <ul class="sub-sub-group-list">
                                    <li><a href="#">3rd level nav item</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            
            <li><a href="<?php echo base_url(); ?>login/logout" class="w3-padding"><i class="fa fa-sign-out"></i>  Sign-out</a></li>
        </ul>
    </nav>
</nav>

<!-- Overlay effect when opening sidenav on small screens -->
<div class="w3-overlay w3-hide-large w3-animate-opacity" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>
           
    