    <!-- !PAGE CONTENT! -->
    <div class="w3-main" style="margin-left:250px;margin-top:43px;">

        <!-- Header -->
        <header class="w3-container" style="padding-top:22px">
            <h5><b><i class="fa fa-dashboard"></i><?php echo isset($title)? " AccSys - $title": NULL;?></b></h5>
        </header>

        <br>
             <div class="w3-container">
        <div class="w3-panel w3-pale-teal w3-bottombar w3-border-teal w3-border">
             <div class="w3-container w3-teal">
                <h4 class="w3-left"><?php echo $form_title;?></h4>
                <h6 class="w3-right"><?php echo isset($msg)? "$msg": NULL;?></h6>
            </div>
            <form class="w3-container" action="<?php echo base_url();?>inventory/insert" method="post">
            <br>
                <div class="w3-row-padding">
                    <div class="w3-col s12 m12 l3">
                        <label>Category ID</label>
                        <input type="hidden" name="company_id" value="<?php echo $this->session->user_data->company_id; ?>" />
                        <input type="hidden" name="table" value="<?php echo $table;?>">
                        <select class="w3-input w3-border w3-round w3-hover-sand" id="category_id" name="category_id" required>
                                <?php print_r($category);?>
                        </select>
                    </div>
                    <div class="w3-col s12 m12 l2">
                        <label>Product ID</label>
                        <input class="w3-input w3-border w3-round w3-hover-sand" type="text" id="product_id" value="" name="product_id" readonly>
                    </div>
                    <div class="w3-col s12 m12 l7">
                        <label>Product Name</label>
                        <input class="w3-input w3-border w3-round w3-hover-sand" type="text" id='product' name="product" required>
                    </div>
                </div>
                <br>
                <div class="w3-row-padding">
                    <div class="w3-col s12 m12 l6">
                        <label>Brand</label>
                        <select class="w3-input w3-border w3-round w3-hover-sand" name="brand_id">
                                <?php print_r($brand);?>
                        </select>
                    </div>
                    <div class="w3-col s12 m12 l6">
                        <label>Unit</label>
                        <select class="w3-input w3-border w3-round w3-hover-sand" name="unit" required>
                                <?php print_r($unit);?>
                        </select>
                    </div>
                </div>
                <br>
                <div class="w3-row-padding">
                    <div class="w3-col s12 m12 l6">
                        <label>Cost Price</label>
                        <input class="w3-input w3-border w3-round w3-hover-sand" type="text" name="cost">
                    </div>
                    <div class="w3-col s12 m12 l6">
                        <label>List Price</label>
                        <input class="w3-input w3-border w3-round w3-hover-sand" type="text" name="price" required>
                    </div>
                </div>
                <br>
                <div class="w3-row-padding">
                    <div class="w3-col s12 m12 l12">
                        <button class="w3-btn w3-teal w3-right" type="submit">Submit</button>
                    </div>
                </div>
                <br>
            </form>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#category_id").focus();
            $("#category_id").change(function () {
                $.ajax({
                    url: "<?php echo base_url(); ?>inventory/getProduct_id",
                    data: {category_id: $(this).val()},
                    type: "POST",
                    success: function (data) {
                        $("#product_id").val(data).change();
                        $("#product").focus();
                    }
                });
            });


        });
    </script>