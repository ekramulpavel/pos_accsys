    <style>
        .tab {display:none;}
    </style>
    
    <!-- !PAGE CONTENT! -->
    <div class="w3-main" style="margin-left:250px;margin-top:43px;">

        <!-- Header -->
        <header class="w3-container" style="padding-top:22px">
            <h5><b><i class="fa fa-dashboard"></i><?php echo isset($title)? " AccSys - $title": NULL;?></b></h5>
        </header>

        <br>
         <div class="w3-container">
            <div class="w3-panel w3-pale-teal w3-bottombar w3-border-teal w3-border">
                <div class="w3-container w3-teal">
                    <h4 class="w3-left"><?php echo $form_title;?></h4>
                    <h6 class="w3-right"><?php echo isset($msg)? "$msg": NULL;?></h6>
                </div>
                
                <div class="w3-row">
                    <a href="#" onclick="openCity(event, 'product_details');">
                        <div class="w3-quarter tablink w3-bottombar w3-hover-light-grey w3-padding">Product Details</div>
                    </a>
                    <a href="#" onclick="openCity(event, 'order_history');">
                        <div class="w3-quarter tablink w3-bottombar w3-hover-light-grey w3-padding">Order HIstory</div>
                    </a>
                    <a href="#" onclick="openCity(event, 'purchase_history');">
                        <div class="w3-quarter tablink w3-bottombar w3-hover-light-grey w3-padding">Purchase History</div>
                    </a>
                    <a href="#" onclick="openCity(event, 'inventory');">
                        <div class="w3-quarter tablink w3-bottombar w3-hover-light-grey w3-padding">Inventory</div>
                    </a>
                </div>

                <div id="product_details" class="w3-container tab">
                    <h2>Product Details</h2>
                    <p>London is the capital city of England.</p>
                </div>

                <div id="order_history" class="w3-container tab">
                    <h2>Order History</h2>
                    <p>Paris is the capital of France.</p>
                </div>

                <div id="purchase_history" class="w3-container tab">
                    <h2>Purchase History</h2>
                    <p>Tokyo is the capital of Japan.</p>
                </div>
                <div id="inventory" class="w3-container tab">
                    <h2>Inventory</h2>
                    <p>Tokyo is the capital of Japan.</p>
                </div>
                <!--
                <form class="w3-container" action="<?php echo base_url();?>inventory/insert" method="post">
                <br>
                    <div class="w3-container w3-teal">
                        <h4 class="w3-left"><?php echo $form_title;?></h4>
                        <h6 class="w3-right"><?php echo isset($msg)? "$msg": NULL;?></h6>
                    </div>
                    <br>
                    <div class="w3-row-padding">
                        <div class="w3-col s12 m12 l4">
                            <label>Category</label>
                            <input type="hidden" name="url" value="<?php echo $this->uri->uri_string(); ?>" />
                            <input class="w3-input w3-border w3-round w3-hover-sand" type="text">
                        </div>
                        <div class="w3-col s12 m12 l4">
                            <label>Start Date</label>
                            <input class="w3-input w3-border w3-round w3-hover-sand" type="date" >
                        </div>
                         <div class="w3-col s12 m12 l4">
                            <label>End Date</label>
                            <input class="w3-input w3-border w3-round w3-hover-sand" type="date" >
                        </div>
                    </div>

                    <br>
                    <div class="w3-row-padding">
                        <div class="w3-col s12 m12 l12">
                            <button class="w3-btn w3-teal w3-right" type="submit">Submit</button>
                        </div>
                    </div>
                    <br>
                </form>
                -->
            </div>
        </div>

     </div>

     <script>
        function openCity(evt, cityName) {
          var i, x, tablinks;
          x = document.getElementsByClassName("tab");
          for (i = 0; i < x.length; i++) {
             x[i].style.display = "none";
          }
          tablinks = document.getElementsByClassName("tablink");
          for (i = 0; i < x.length; i++) {
             tablinks[i].className = tablinks[i].className.replace(" w3-border-teal", "");
          }
          document.getElementById(cityName).style.display = "block";
          evt.currentTarget.firstElementChild.className += " w3-border-teal";
        }
    </script>


                                                                                                                             