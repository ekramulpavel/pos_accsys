    <!-- !PAGE CONTENT! -->
    <div class="w3-main" style="margin-left:250px;margin-top:43px;">

        <!-- Header -->
        <header class="w3-container" style="padding-top:22px">
            <h5><b><i class="fa fa-dashboard"></i><?php echo isset($title)? " AccSys - $title": NULL;?></b></h5>
        </header>

        <br>
         <div class="w3-container">
            <div class="w3-panel w3-pale-teal w3-bottombar w3-border-teal w3-border">
                <div class="w3-container w3-teal">
                    <h4 class="w3-left"><?php echo $form_title.': Total Trade-';?><span id="total_trade"><?php echo count((array)$trade_view)?></span></h4>
                    <h6 class="w3-right"><?php echo isset($msg)? "$msg": NULL;?></h6>
                </div>
                <!--<form class="w3-container" action="" method="post">-->
                     <br>
                <div class="w3-row-padding">
                    <div class="w3-col s12 m4 l4">
                        <label>Trade</label>
                        <input type="hidden" name="table" id="table" value="<?php echo $table;?>">
                        <select class="w3-input w3-border w3-round w3-hover-sand param" id="trade" name="trade">
                                <?php print_r($trade);?>
                        </select>
                    </div>
                    <div class="w3-col s12 m8 l8">
                        <label>String Search</label>
                        <input class="w3-input w3-border w3-round w3-hover-sand param" type="text" id="str_src" value="" name="search_string">
                    </div>
                </div>              
                <br>
                <!--</form>-->
            </div>
            <div id="existing-data">
                <table class="w3-table w3-bordered w3-striped">
                    <thead>
                         <tr class="w3-teal">
                            <th>Trade ID</th>
                            <th>Trade Name</th>
                            <?php if($this->session->user_data->user_role!=4):?>
                            <th>Action</th>
                            <?php endif ?>
                        </tr>
                    </thead>
                   <tbody>
                   <?php foreach($trade_view as $key => $value):?>
                        <tr>
                            <td><?php echo $value->trade_id;?></td>
                            <td><?php echo $value->trade;?></td>
                            <?php if($this->session->user_data->user_role!=4):?>
                            <td>
                                <a href="<?php echo base_url().'inventory/edit/'.$table.'/'.$value->trade_id; ?>" class="fa fa-edit" style="text-decoration:none;"></a> &nbsp;|&nbsp;
                                <a href="<?php echo base_url().'inventory/del/'.$table.'/'.$value->trade_id; ?>" class="fa fa-trash-o delete" style="text-decoration:none;"></a>
                            </td>
                            <?php endif ?>
                        </tr>
                    <?php endforeach;?>
                    
                    </tbody>
                </table>
            </div>

            <div id="param-data">
                
            </div>
        </div>

    <script type="text/javascript">
        $(document).ready(function () {
        
            $(".param").change(function () {
                
                var dataString = {
                    trade_id: $('#trade').val(),
                    str_src: $('#str_src').val()
                    
                };
                var jsonString = JSON.stringify(dataString);

                $.ajax({
                    url: "<?php echo base_url(); ?>inventory/viewTradeByParam",
                    data: {data: jsonString},
                    type: "POST",
                    dataType: 'json',
                    success: function (data) {
                        tabledata(data);
                        //alert(data);
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                        alert("some error");
                    }
                });

            });

            function tabledata(data){
                $('#existing-data').hide();
                var table_name=$('#table').val();

                var html =  '<table class="w3-table w3-bordered w3-striped" id="param-data"><thead><tr class="w3-teal">'+
                            '<th>Trade ID</th>'+
                            '<th>Trade Name</th>'+
                            '<th>Action</th>';
                        
                $.each(data, function(index, item) {
                    html +='<tr>';
                    html +='<td>'+ item.trade_id + '</td>';
                    html +='<td>'+ item.trade + '</td>';
                    html +='<td>'+ '<a href="<?php echo base_url().'inventory/edit_client/';?>'+table_name+'/'+item.trade_id+'" class="fa fa-edit" style="text-decoration:none;"></a> &nbsp;|&nbsp;'+
                                   '<a href="<?php echo base_url().'inventory/del/';?>'+table_name+'/'+item.trade_id+'" class="fa fa-trash-o delete" style="text-decoration:none;"></a>';
                    html +='</td></tr>';
                });

                html += '</tbody></table>';
                $('#total_trade').text(data.length);
                $('#param-data').html(html);
            }

           
            $(document).on('click','.delete', function(e){
                if(!confirm('Are you sure?')){
                    e.preventDefault();
                    return false;
                }
                return true;
            });

        });
    </script>