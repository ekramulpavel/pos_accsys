    <!-- !PAGE CONTENT! -->
    <div class="w3-main" style="margin-left:250px;margin-top:43px;">

        <!-- Header -->
        <header class="w3-container" style="padding-top:22px">
            <h5><b><i class="fa fa-dashboard"></i><?php echo isset($title)? " AccSys - $title": NULL;?></b></h5>
        </header>

        <br>
        <div class="w3-container">
        <div class="w3-panel w3-pale-teal w3-bottombar w3-border-teal w3-border">
            <div class="w3-container w3-teal">
                <h4 class="w3-left"><?php echo $form_title;?></h4>
                <h6 class="w3-right"><?php echo isset($msg)? "$msg": NULL;?></h6>
            </div>
            <form class="w3-container" action="<?php echo base_url();?>inventory/insert" method="post">
            <br>
                <div class="w3-row-padding">
                    <div class="w3-col s12 m12 l3">
                        <label>Category ID</label>
                        <input type="hidden" name="url" value="<?php echo $this->uri->uri_string(); ?>" />
                        <select class="w3-input w3-border w3-round w3-hover-sand" id="category_id" name="category_id">
                                <?php print_r($category);?>
                        </select>
                    </div>
                     <div class="w3-col s12 m12 l7">
                        <label>Product Name</label>
                        <select class="w3-input w3-border w3-round w3-hover-sand" id="product_list" name="product_list" required>
                                <?php print_r($product_list);?>
                        </select>
                    </div>
                    <div class="w3-col s12 m12 l2">
                        <label>Product ID</label>
                        <input class="w3-input w3-border w3-round w3-hover-sand" type="text" id="product_id" value="" name="product_id">
                    </div>
                </div>
                <br>
                <div class="w3-row-padding">
                    <div class="w3-col s12 m12 l12">
                        <button class="w3-btn w3-teal w3-right" type="submit">Submit</button>
                    </div>
                </div>
                <br>
            </form>
        </div>
        <div>
            <table class="w3-table w3-bordered w3-striped">
                <thead>
                    <tr class="w3-teal">
                        <th>Product ID</th>
                        <th>Product</th>
                        <th>Brand</th>
                        <th>Unit</th>
                        <th>Price</th>
                        <th>Category</th>
                        <th>Active</th>
                        <?php if($this->session->user_data->user_role!=4){?>
                        <th>Action</th>
                        <?php } ?>
                    </tr>
                </thead>
                <tbody>
                <?php
                    foreach ($products as $key => $value) {
                ?>
                    <tr>
                        <td><?php echo $value->product_id ?></td>
                        <td><?php echo $value->product ?></td>
                        <td><?php echo $value->brand_id ?></td>
                        <td><?php echo $value->unit ?></td>
                        <td><?php echo $value->price ?></td>
                        <td><?php echo $value->category_id ?></td>
                        <td><?php echo $value->discontinue ?></td>
                        <?php 
                            if($this->session->user_data->user_role!=4){
                        ?>
                            <td>
                            <a href="<?php echo base_url().'billing/edit_client/'.$value->product_id; ?>" class="fa fa-edit" style="text-decoration:none;"></a> &nbsp;|&nbsp;
                            <a id="del" href="<?php echo base_url().'billing/del_client/'.$value->product_id; ?>" class="fa fa-trash-o" style="text-decoration:none;"></a>
                            </td>
                        <?php
                            }
                        ?>
                            </tr>
                <?php       
                    }
                ?>
                </tbody>
                <tfoot>
                   
                </tfoot>
            </table>
        </div>

        <div id="load-data">
            <?php echo isset($data->products)? $data->products:""; ?> 
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
        
            $("#category_id").change(function () {
                $.ajax({
                    url: "<?php echo base_url(); ?>inventory/getProductListByCategory",
                    data: {category_id: $(this).val()},
                    type: "POST",
                    datatype:'json',
                    success: function (data) {
                        var obj = jQuery.parseJSON(data);
                        $("#product_list").html(data);
                        $("#load-data").html(data);
                        //alert(data);
                    }
                });
            });

        });
    </script>