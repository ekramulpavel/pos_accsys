    <!-- !PAGE CONTENT! -->
    <div class="w3-main" style="margin-left:250px;margin-top:43px;">

        <!-- Header -->
        <header class="w3-container" style="padding-top:22px">
            <h5><b><i class="fa fa-dashboard"></i><?php echo isset($title)? " AccSys - $title": NULL;?></b></h5>
        </header>

         <br>

        <div class="w3-container">
            <form class="w3-container">
                <div class="w3-row-padding">
                    <div class="w3-col s9">
                        <label class="w3-label w3-text-teal"><b>ID</b></label>
                        <input class="w3-input w3-border w3-round" type="text">
                    </div>
                    
                    <div class="w3-col s3">
                        <label class="w3-label w3-text-teal"><b>Order Date</b></label>
                        <input class="w3-input w3-border w3-round" type="text">
                    </div>
                </div>

        <!-- Footer -->
        <footer class="w3-container w3-light-grey w3-text-blue">
            <p>Powered by <a href="http://vividwork.com" target="_blank">vividwork.com</a></p>
        </footer>

      <!-- End page content -->
    </div>