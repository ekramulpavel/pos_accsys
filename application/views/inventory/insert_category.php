    <!-- !PAGE CONTENT! -->
    <div class="w3-main" style="margin-left:250px;margin-top:43px;">

        <!-- Header -->
        <header class="w3-container" style="padding-top:22px">
            <h5><b><i class="fa fa-dashboard"></i><?php echo isset($title)? " AccSys - $title": NULL;?></b></h5>
        </header>
        <div class="w3-container">
            <div class="w3-panel w3-pale-teal w3-bottombar w3-border-teal w3-border">
                <div class="w3-container w3-teal">
                    <h4 class="w3-left"><?php echo $form_title;?></h4>
                    <h6 class="w3-right"><?php echo isset($msg)? "$msg": NULL;?></h6>
                </div>
                <form class="w3-container" action="<?php echo base_url();?>inventory/insert" method="post">
                <br>
                    
                    <div class="w3-row-padding">
                        <div class="w3-col s12 m12 l3">
                            <label>Category ID</label>
                            <input type="hidden" name="company_id" value="<?php echo $this->session->user_data->company_id; ?>" />
                            <input type="hidden" name="table" value="<?php echo $table;?>">
                            <input class="w3-input w3-border w3-round w3-hover-sand" type="text" value="<?php echo isset($max->id)? $max->id+1: $max->id=1;?>" name="category_id" readonly>
                        </div>
                        <div class="w3-col s12 m12 l9">
                            <label>Category</label>
                            <input class="w3-input w3-border w3-round w3-hover-sand" type="text" id="category" name="category" required>
                        </div>
                    </div>
                    <div class="w3-row-padding">
                        <div class="w3-col s12 m12 l12">
                            <label>Trade</label>
                            <select class="w3-input w3-border w3-round w3-hover-sand" name="trade_id" required>
                                <?php print_r($trade);?>
                            </select>
                        </div>
                    </div>
                    <br>
                    <div class="w3-row-padding">
                        <div class="w3-col s12 m12 l12">
                            <button class="w3-btn w3-teal w3-right" type="submit">Submit</button>
                        </div>
                    </div>
                    <br>
                </form>
            </div>
        </div>

      <!-- End page content -->
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#category').focus();
        });
    </script>