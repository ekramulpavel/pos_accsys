<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	public $layout;

    function __construct() {
        parent::__construct();
        //$data['msg'] = $this->session->flashdata('usermsg');
        $this->layout='layout/master';
    }
}